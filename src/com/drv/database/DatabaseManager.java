package com.drv.database;

import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.drv.beans.BusinessProfile;
import com.drv.beans.CardInfo;
import com.drv.beans.CountryInfo;
import com.drv.beans.CustomerInfo;
import com.drv.beans.Product;
import com.drv.beans.Taxes;
import com.drv.beans.TaxesParam;
import com.drv.beans.Tips;
import com.drv.beans.Transaction;
import com.drv.database.DatabaseDefinition.Business_Profile;
import com.drv.database.DatabaseDefinition.Card_Info;
import com.drv.database.DatabaseDefinition.Customer;
import com.drv.database.DatabaseDefinition.ProductDB;
import com.drv.database.DatabaseDefinition.Taxes_Param;
import com.drv.database.DatabaseDefinition.Transaction_History;

public class DatabaseManager {
	private static DatabaseManager mDbManagerInstance;
	private static DatabaseHelper mDbHelper;
	private static final String LOG_TAG = "DatabaseManager";

	private DatabaseManager() {
	}

	public static DatabaseManager getInstance(Context context) {
		if (mDbManagerInstance == null) {
			mDbManagerInstance = new DatabaseManager();
			mDbHelper = new DatabaseHelper(context);
		}
		return mDbManagerInstance;
	}

	public boolean isEmailExisted(String email) {
		boolean result = false;
		Cursor c = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM CUSTOMER WHERE EMAIL='"
						+ email + "'";
				c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					result = true;
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return result;
	}

	public boolean registerCustomer(CustomerInfo customerInfo) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Customer.C_FIRST_NAME, customerInfo.getFirstname());
					ctv.put(Customer.C_LAST_NAME, customerInfo.getLastname());
					ctv.put(Customer.C_EMAIL, customerInfo.getEmail());
					ctv.put(Customer.C_PASSWORD, customerInfo.getPassword());
					ctv.put(Customer.C_BALANCE, customerInfo.getBalance());
					long id = db.insert(Customer.TABLE_NAME, null, ctv);
					if (id > -1) {
						customerInfo.setId(id);
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public CustomerInfo customerByEmail(String email) {
		CustomerInfo customerInfo = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM CUSTOMER WHERE (EMAIL = '"
						+ email + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() == 1) {
					customerInfo = new CustomerInfo();
					// email
					customerInfo.setEmail(email);
					while (c.moveToNext()) {
						// firstname
						customerInfo.setFirstname(c.getString(c
								.getColumnIndex(Customer.C_FIRST_NAME)));
						// lastname
						customerInfo.setLastname(c.getString(c
								.getColumnIndex(Customer.C_LAST_NAME)));
						// password
						customerInfo.setPassword(c.getString(c
								.getColumnIndex(Customer.C_PASSWORD)));
						// pin
						customerInfo.setPin(c.getString(c
								.getColumnIndex(Customer.C_PIN)));
						// card number
						customerInfo.setCardNumber(c.getString(c
								.getColumnIndex(Customer.C_CARD_NUMBER)));
						// country of mobile
						customerInfo.setCountryOfMobile(c.getString(c
								.getColumnIndex(Customer.C_COUNTRY_OF_MOBILE)));
						// id
						customerInfo.setId(c.getLong(c
								.getColumnIndex(Customer.C_ID)));
						// mobile phone
						customerInfo.setMobilePhone(c.getString(c
								.getColumnIndex(Customer.C_MOBILE_PHONE)));
						// balance
						customerInfo.setBalance(c.getFloat(c
								.getColumnIndex(Customer.C_BALANCE)));
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return customerInfo;
	}

	public boolean updatePersonalInformation(CustomerInfo newPersonalInfo) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Customer.C_FIRST_NAME,
							newPersonalInfo.getFirstname());
					ctv.put(Customer.C_LAST_NAME, newPersonalInfo.getLastname());

					int numOfRow = db
							.update(Customer.TABLE_NAME, ctv, Customer.C_ID
									+ "=" + newPersonalInfo.getId(), null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean updateContactInformation(CustomerInfo newContactInfo) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Customer.C_EMAIL, newContactInfo.getEmail());
					ctv.put(Customer.C_COUNTRY_OF_MOBILE,
							newContactInfo.getCountryOfMobile());
					ctv.put(Customer.C_MOBILE_PHONE,
							newContactInfo.getMobilePhone());
					int numOfRow = db.update(Customer.TABLE_NAME, ctv,
							Customer.C_ID + "=" + newContactInfo.getId(), null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean updatePassword(long userId, String newPassword) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Customer.C_PASSWORD, newPassword);
					int numOfRow = db.update(Customer.TABLE_NAME, ctv,
							Customer.C_ID + "=" + userId, null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean updateBalance(long userId, float balance) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Customer.C_BALANCE, balance);
					int numOfRow = db.update(Customer.TABLE_NAME, ctv,
							Customer.C_ID + "=" + userId, null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public ArrayList<CountryInfo> getAllCountry() {
		ArrayList<CountryInfo> result = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT COUNTRY_NAME, ITU_T_TELEPHONE_CODE, ISO_2_LETTER_CODE FROM COUNTRIES ORDER BY COUNTRY_NAME";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					result = new ArrayList<CountryInfo>();
					while (c.moveToNext()) {
						CountryInfo ci = new CountryInfo();
						ci.setName(c.getString(0));
						String rawDialing = c.getString(1);
						ci.setDialingCode(rawDialing.replace("-", ""));
						ci.setIso2(c.getString(2));
						ci.setIso3(c.getString(3));
						result.add(ci);
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return result;
	}

	public Cursor getAllCountryToCursor(String arg) {
		Cursor result = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery;
				if (arg == null) {
					rawQuery = "SELECT COUNTRY_NAME, IMAGE, ITU_T_TELEPHONE_CODE, ISO_2_LETTER_CODE FROM COUNTRIES ORDER BY COUNTRY_NAME";
				} else {
					rawQuery = "SELECT COUNTRY_NAME, IMAGE, ITU_T_TELEPHONE_CODE, ISO_2_LETTER_CODE FROM COUNTRIES WHERE COUNTRY_NAME LIKE '%"
							+ arg + "%'" + "ORDER BY COUNTRY_NAME";
				}
				result = db.rawQuery(rawQuery, null);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return result;
	}

	public ArrayList<Product> getAllProduct() {
		ArrayList<Product> result = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESC, AMOUNT, PRODUCT_IMG FROM PRODUCT ORDER BY PRODUCT_ID";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					result = new ArrayList<Product>();
					while (c.moveToNext()) {
						Product product = new Product();
						product.setProductId(c.getInt(0));
						product.setProductName(c.getString(1));
						product.setProductDesc(c.getString(2));
						product.setAmount(c.getFloat(3));
						product.setProductImage(c.getBlob(4));
						result.add(product);
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return result;
	}

	public boolean addNewProduct(Product product) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(ProductDB.C_PRODUCT_NAME, product.getProductName());
					ctv.put(ProductDB.C_PRODUCT_DESC, product.getProductDesc());
					ctv.put(ProductDB.C_AMOUNT, product.getAmount());
					ctv.put(ProductDB.C_PRODUCT_IMG, product.getProductImage());
					long id = db.insert(ProductDB.TABLE_NAME, null, ctv);
					if (id > -1) {
						result = true;
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public ArrayList<Transaction> getTransactionListByCustomerId(long userId) {
		ArrayList<Transaction> transactionList = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TRANSACTION_HISTORY WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					transactionList = new ArrayList<Transaction>();
					while (c.moveToNext()) {

						Transaction tran = new Transaction();
						tran.setOrderId(c.getInt(c
								.getColumnIndex(Transaction_History.C_ORDER_ID)));
						tran.setTransactionDate(new Date(
								c.getLong(c
										.getColumnIndex(Transaction_History.C_TRANSACTION_DATE))));
						tran.setTotalAmount(c.getFloat(c
								.getColumnIndex(Transaction_History.C_TOTAL_AMOUNT)));
						tran.setPaymentMethod(c.getString(c
								.getColumnIndex(Transaction_History.C_PAYMENT_METHOD)));
						tran.setTaxValue(c.getFloat(c
								.getColumnIndex(Transaction_History.C_TAXES_RATE)));
						tran.setTipValue(c.getFloat(c
								.getColumnIndex(Transaction_History.C_TIP_VALUE)));
						tran.setSubTotal(c.getFloat(c
								.getColumnIndex(Transaction_History.C_SUBTOTAL)));
						tran.setStatus(c.getInt(c
								.getColumnIndex(Transaction_History.C_STATUS)) == 0 ? false
								: true);

						transactionList.add(tran);
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return transactionList;
	}

	public boolean addNewTransaction(Transaction tran, long userId) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Transaction_History.C_ORDER_ID, tran.getOrderId());
					ctv.put(Transaction_History.C_CUSTOMER_ID, userId);
					ctv.put(Transaction_History.C_TRANSACTION_DATE, tran
							.getTransactionDate().getTime());
					ctv.put(Transaction_History.C_PAYMENT_METHOD,
							tran.getPaymentMethod());
					ctv.put(Transaction_History.C_PRODUCT_ID,
							tran.getProductId());
					ctv.put(Transaction_History.C_QTY, tran.getQty());
					ctv.put(Transaction_History.C_TIP_VALUE, tran.getTipValue());
					ctv.put(Transaction_History.C_TAXES_RATE,
							tran.getTaxValue());
					ctv.put(Transaction_History.C_SUBTOTAL, tran.getSubTotal());
					ctv.put(Transaction_History.C_TOTAL_AMOUNT,
							tran.getTotalAmount());
					ctv.put(Transaction_History.C_STATUS, tran.isStatus());

					long id = db.insert(Transaction_History.TABLE_NAME, null,
							ctv);
					if (id > -1) {
						result = true;
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public ArrayList<TaxesParam> getTaxesParam() {
		ArrayList<TaxesParam> taxesParamList = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TAXES_PARAM";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					taxesParamList = new ArrayList<TaxesParam>();
					while (c.moveToNext()) {

						TaxesParam taxesParam = new TaxesParam();
						taxesParam.setCountry(c.getString(c
								.getColumnIndex(Taxes_Param.C_COUNTRY)));
						taxesParam.setProvince(c.getString(c
								.getColumnIndex(Taxes_Param.C_PROVINCE)));
						taxesParam.setRate(c.getInt(c
								.getColumnIndex(Taxes_Param.C_RATE)));

						taxesParamList.add(taxesParam);
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return taxesParamList;
	}

	public boolean updateTaxesRate(long userId, Taxes taxes) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_COUNTRY,
							taxes.getCountry());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_PROVINCE,
							taxes.getProvince());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_RATE,
							taxes.getRate());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_IS_ENABLE,
							taxes.isEnable());

					int numOfRow = db
							.update(com.drv.database.DatabaseDefinition.Taxes.TABLE_NAME,
									ctv,
									com.drv.database.DatabaseDefinition.Taxes.C_CUSTOMER_ID
											+ "=" + userId, null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean updateTipValue(long userId, Tips tips) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_TYPE,
							tips.getDefaultType());
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_VALUE,
							tips.getDefaultType());
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_IS_ENABLE,
							tips.isEnable());

					int numOfRow = db
							.update(com.drv.database.DatabaseDefinition.Tips.TABLE_NAME,
									ctv,
									com.drv.database.DatabaseDefinition.Tips.C_CUSTOMER_ID
											+ "=" + userId, null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean chkExistTaxesRate(long userId) {
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TAXES WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					return true;
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return false;
	}

	public boolean chkExistTipValue(long userId) {
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TIPS WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					return true;
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return false;
	}

	public Taxes getTaxes(long userId) {
		Taxes taxes = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TAXES WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					taxes = new Taxes();
					taxes.setCountry(c.getString(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Taxes.C_COUNTRY)));
					taxes.setProvince(c.getString(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Taxes.C_PROVINCE)));
					taxes.setRate(c.getFloat(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Taxes.C_RATE)));
					taxes.setEnable(c.getInt(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Taxes.C_IS_ENABLE)) == 1 ? true
							: false);
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return taxes;
	}

	public Tips getTips(long userId) {
		Tips tips = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM TIPS WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					tips = new Tips();
					tips.setDefaultType(c.getString(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_TYPE)));
					tips.setDefaultValue(c.getInt(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_VALUE)));
					tips.setEnable(c.getInt(c
							.getColumnIndex(com.drv.database.DatabaseDefinition.Tips.C_IS_ENABLE)) == 1 ? true
							: false);
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return tips;
	}

	public boolean addTips(Tips tip, long userId) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_CUSTOMER_ID,
							userId);
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_TYPE,
							tip.getDefaultType());
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_DEFAULT_VALUE,
							tip.getDefaultValue());
					ctv.put(com.drv.database.DatabaseDefinition.Tips.C_IS_ENABLE,
							tip.isEnable());

					long id = db
							.insert(com.drv.database.DatabaseDefinition.Tips.TABLE_NAME,
									null, ctv);
					if (id > -1) {
						result = true;
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean addTaxes(Taxes tax, long userId) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_CUSTOMER_ID,
							userId);
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_COUNTRY,
							tax.getCountry());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_PROVINCE,
							tax.getProvince());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_RATE,
							tax.getRate());
					ctv.put(com.drv.database.DatabaseDefinition.Taxes.C_IS_ENABLE,
							tax.isEnable());

					long id = db
							.insert(com.drv.database.DatabaseDefinition.Taxes.TABLE_NAME,
									null, ctv);
					if (id > -1) {
						result = true;
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public ArrayList<CardInfo> getAllCardInfoByCustomerId(long userId) {
		ArrayList<CardInfo> cardInfoList = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM CARD_INFO WHERE (CUSTOMER_ID = '"
						+ userId + "');";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					cardInfoList = new ArrayList<CardInfo>();
					while (c.moveToNext()) {

						CardInfo carInfo = new CardInfo();
						carInfo.setCardName(c.getString(c
								.getColumnIndex(Card_Info.C_CARD_NAME)));
						carInfo.setCardNumber(c.getString(c
								.getColumnIndex(Card_Info.C_CARD_NUMBER)));
						carInfo.setCardType(c.getString(c
								.getColumnIndex(Card_Info.C_CARD_TYPE)));

						cardInfoList.add(carInfo);
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return cardInfoList;
	}

	public BusinessProfile getBusninessProfile(long customerId) {
		BusinessProfile profile = null;
		if (mDbHelper != null) {
			try {
				SQLiteDatabase db = mDbHelper.getReadableDatabase();
				String rawQuery = "SELECT * FROM BUSINESS_PROFILE WHERE CUSTOMER_ID = '"
						+ customerId + "'";
				Cursor c = db.rawQuery(rawQuery, null);
				if (c.getCount() > 0) {
					profile = new BusinessProfile();
					while (c.moveToNext()) {
						// ID
						profile.setId(c.getLong(c
								.getColumnIndex(Business_Profile.C_ID)));
						// Business Name
						profile.setName(c.getString(c
								.getColumnIndex(Business_Profile.C_NAME)));
						// Business Number
						profile.setNumber(c.getString(c
								.getColumnIndex(Business_Profile.C_NUMBER)));
						// Country
						profile.setCountry(c.getString(c
								.getColumnIndex(Business_Profile.C_COUNTRY)));
						// Province
						profile.setProvince(c.getString(c
								.getColumnIndex(Business_Profile.C_PROVINCE)));
						// Address
						profile.setAddress(c.getString(c
								.getColumnIndex(Business_Profile.C_ADDRESS)));
						// Phone Number
						profile.setPhoneNumber(c.getString(c
								.getColumnIndex(Business_Profile.C_PHONE_NUMBER)));
						// Postal Code
						profile.setPostalCode(c.getString(c
								.getColumnIndex(Business_Profile.C_POSTAL_CODE)));
						// City
						profile.setCity(c.getString(c
								.getColumnIndex(Business_Profile.C_CITY)));
					}
				}
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (CursorIndexOutOfBoundsException e) {
				e.toString();
			}
		}
		return profile;
	}

	public boolean updateBusinessProfile(BusinessProfile profile) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Business_Profile.C_NAME, profile.getName());
					ctv.put(Business_Profile.C_NUMBER, profile.getNumber());
					ctv.put(Business_Profile.C_COUNTRY, profile.getCountry());
					ctv.put(Business_Profile.C_PROVINCE, profile.getProvince());
					ctv.put(Business_Profile.C_ADDRESS, profile.getAddress());
					ctv.put(Business_Profile.C_CITY, profile.getCity());
					ctv.put(Business_Profile.C_POSTAL_CODE,
							profile.getPostalCode());
					ctv.put(Business_Profile.C_PHONE_NUMBER,
							profile.getPhoneNumber());

					int numOfRow = db
							.update(Business_Profile.TABLE_NAME,
									ctv,
									Business_Profile.C_ID + "="
											+ profile.getId(), null);
					if (numOfRow != 1) {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}

	public boolean addBusinessProfile(BusinessProfile profile) {
		synchronized (this) {
			SQLiteDatabase db = null;
			boolean result = true;
			if (mDbHelper == null) {
				result = false;
			}
			try {
				db = mDbHelper.getWritableDatabase();
				db.beginTransaction();
				{
					ContentValues ctv = new ContentValues();
					ctv.put(Business_Profile.C_NAME, profile.getName());
					ctv.put(Business_Profile.C_NUMBER, profile.getNumber());
					ctv.put(Business_Profile.C_COUNTRY, profile.getCountry());
					ctv.put(Business_Profile.C_PROVINCE, profile.getProvince());
					ctv.put(Business_Profile.C_ADDRESS, profile.getAddress());
					ctv.put(Business_Profile.C_CITY, profile.getCity());
					ctv.put(Business_Profile.C_POSTAL_CODE,
							profile.getPostalCode());
					ctv.put(Business_Profile.C_PHONE_NUMBER,
							profile.getPhoneNumber());
					ctv.put(Business_Profile.C_CUSTOMER_ID,
							profile.getCustomerId());

					long id = db.insert(Business_Profile.TABLE_NAME, null, ctv);
					if (id > -1) {
						profile.setId(id);
					} else {
						result = false;
					}
				}
				db.setTransactionSuccessful();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (db != null) {
					db.endTransaction();
				}
			}
			return result;
		}
	}
}
