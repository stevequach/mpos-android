package com.drv.database;

public class DatabaseDefinition {
    public static final String CREATE_SQL = "createTableSql";
    public static final String UPDATE_SQL = "updateTableSql";

    public static final int VERSION = 1;
    public static final String FILE = "pos.db";
    public static final String DESC = " DESC ";
    public static final String ASC = " ASC "; 

    private final static String CREATE_TABLE = "CREATE TABLE ";    
    private final static String T_LONG = "LONG  ";    
    private final static String T_INTEGER = " INTEGER ";
    private final static String T_FLOAT = " FLOAT ";
    private final static String T_VARCHAR = " VARCHAR ";
    private final static String T_BOOLEAN = " BOOLEAN ";
    private final static String T_TEXT = " TEXT ";
    private final static String T_REAL = " REAL "; 
    private final static String T_BLOB = " BLOB ";    
    private final static String T_NOT_NULL = " NOT NULL "; 
    private final static String T_NULL = " NULL ";
    private final static String T_PRIMARY_KEY = " PRIMARY KEY ";
    private final static String T_DEFAULT = " DEFAULT ";
    private final static String T_AUTOINCREMENT = " AUTOINCREMENT ";

    /** tables in dabatase */
    public static final Class<?>[] Tables = {
    	User.class,
    	Question.class,
    	Test.class,
    	Test_Data.class
    };

    public static final class User{
    	public static final String TABLE_NAME = "user_table";
    	public static final String C_ID = "_id";
    	public static final String C_NAME = "name";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + " AUTOINCREMENT" +
    	    "," + 
		    C_NAME + T_TEXT + T_NOT_NULL +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Question{
    	public static final String TABLE_NAME = "question_table";
    	public static final String C_ID = "_id";
    	public static final String C_CONTENT = "content";
    	public static final String C_OPTION_A = "a";
    	public static final String C_OPTION_B = "b";
    	public static final String C_OPTION_C = "c";
    	public static final String C_OPTION_D = "d";
    	public static final String C_OPTION_E = "e";
    	public static final String C_OPTION_F = "f";
    	public static final String C_OPTION_G = "g";
    	public static final String C_ANSWER = "answers";
    	public static String getOptionAtIndex(int index){
    		String result = C_OPTION_A;
    		if(index == 1){
    			result = C_OPTION_B;
    		}
    		else if(index == 2){
    			result = C_OPTION_C;
    		}
    		else if(index == 3){
    			result = C_OPTION_D;
    		}
    		else if(index == 4){
    			result = C_OPTION_E;
    		}
    		else if(index == 5){
    			result = C_OPTION_F;
    		}
    		else if(index == 6){
    			result = C_OPTION_G;
    		}
    		return result;
    	}
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + " AUTOINCREMENT" +
    	    "," + 
		    C_CONTENT + T_TEXT + T_NOT_NULL +
		    "," + 
		    C_OPTION_A + T_TEXT +
		    "," +
		    C_OPTION_B + T_TEXT +
		    "," +
		    C_OPTION_C + T_TEXT +
		    "," +
		    C_OPTION_D + T_TEXT +
		    "," +
		    C_OPTION_E + T_TEXT +
		    "," +
		    C_OPTION_F + T_TEXT +
		    "," +
		    C_OPTION_G + T_TEXT +
		    "," +
		    C_ANSWER +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Test{
    	public static final String TABLE_NAME = "test_table";
    	public static final String C_ID = "_id";
    	public static final String C_USER_ID = "user_id";
    	public static final String C_SCORE = "score";
    	public static final String C_DATE = "date";
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + " AUTOINCREMENT" +
    	    "," + 
		    C_USER_ID + T_INTEGER +
		    "," + 
		    C_SCORE + T_INTEGER +
		    "," + 
		    C_DATE + T_TEXT +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Test_Data{
    	public static final String TABLE_NAME = "test_data_table";
    	public static final String C_ID = "_id";
    	public static final String C_TEST_ID = "test_id";
    	public static final String C_QUESTION_ID = "question_id";
    	public static final String C_USER_ANSWER = "user_answer";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + " AUTOINCREMENT" +
    	    "," + 
		    C_TEST_ID + T_INTEGER +
		    "," + 
		    C_QUESTION_ID +T_INTEGER +
		    "," + 
		    C_USER_ANSWER +T_TEXT +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Bank_Account{
    	public static final String TABLE_NAME = "BANK_ACCOUNT";
    	public static final String C_ID = "_ID";
    	public static final String C_ACCOUNT_HOLDER = "ACCOUNT_HOLDER";
    	public static final String C_TRANSIT_NUMBER = "TRANSIT_NUMBER";
    	public static final String C_INSTITUTION_NUMBER = "INSTITUTION_NUMBER";
    	public static final String C_ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    	public static final String C_AUTO_DEPOSIT = "AUTO_DEPOSIT";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT +
    	    "," + 
		    C_ACCOUNT_HOLDER + T_TEXT +
		    "," + 
		    C_TRANSIT_NUMBER +T_TEXT +
		    "," + 
		    C_INSTITUTION_NUMBER +T_TEXT +
		    "," +
		    C_ACCOUNT_NUMBER + T_TEXT +
		    "," +
		    C_AUTO_DEPOSIT +T_INTEGER + T_DEFAULT + "0" +
		    "," +
		    C_CUSTOMER_ID + T_INTEGER + 
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Business_Profile{
    	public static final String TABLE_NAME = "BUSINESS_PROFILE";
    	public static final String C_ID = "_ID";
    	public static final String C_NAME = "NAME";
    	public static final String C_NUMBER = "NUMBER";
    	public static final String C_COUNTRY = "COUNTRY";
    	public static final String C_PROVINCE = "PROVINCE";
    	public static final String C_ADDRESS = "ADDRESS";
    	public static final String C_CITY = "CITY";
    	public static final String C_POSTAL_CODE = "POSTAL_CODE";
    	public static final String C_PHONE_NUMBER = "PHONE_NUMBER";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT +
    	    "," + 
		    C_NAME + T_TEXT +
		    "," + 
		    C_NUMBER +T_TEXT +
		    "," + 
		    C_COUNTRY +T_TEXT +
		    "," +
		    C_PROVINCE + T_TEXT +
		    "," +
		    C_ADDRESS + T_TEXT +
		    "," +
		    C_CITY + T_TEXT +
		    "," +
		    C_POSTAL_CODE + T_TEXT +
		    "," +
		    C_PHONE_NUMBER + T_TEXT +
		    "," +
		    C_CUSTOMER_ID + T_INTEGER + 
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Countries{
    	public static final String TABLE_NAME = "COUNTRIES";
    	public static final String C_SORT_ORDER = "SORT_ORDER";
    	public static final String C_COUNTRY_NAME = "COUNTRY_NAME";
    	public static final String C_ISO_CURRENCY_CODE = "ISO_CURRENCY_CODE";
    	public static final String C_ISO_CURRENCY_NAME = "ISO_CURRENCY_NAME";
    	public static final String C_ITU_T_TELEPHONE_CODE = "ITU_T_TELEPHONE_CODE";
    	public static final String C_ISO_2_LETTER_CODE = "ISO_2_LETTER_CODE";
    	public static final String C_ISO_3_LETTER_CODE = "ISO_3_LETTER_CODE";
    	public static final String C_ISO_COUNTRY_NUMBER_CODE = "ISO_COUNTRY_NUMBER_CODE";
    	public static final String C_BLACKLISTED = "BLACKLISTED";
    	public static final String C_IMAGE = "IMAGE";
    	public static final String C_WHITELISTED = "WHITELISTED";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_SORT_ORDER + T_INTEGER +
    	    "," + 
		    C_COUNTRY_NAME + T_VARCHAR + T_NOT_NULL +
		    "," + 
		    C_ISO_CURRENCY_CODE +T_VARCHAR + T_NOT_NULL + 
		    "," + 
		    C_ISO_CURRENCY_NAME +T_VARCHAR + T_NOT_NULL +
		    "," +
		    C_ITU_T_TELEPHONE_CODE + T_VARCHAR + T_NOT_NULL +
		    "," +
		    C_ISO_2_LETTER_CODE + T_VARCHAR + T_NOT_NULL + 
		    "," +
		    C_ISO_3_LETTER_CODE + T_VARCHAR +T_NOT_NULL +
		    "," +
		    C_ISO_COUNTRY_NUMBER_CODE + T_VARCHAR + T_NOT_NULL +
		    "," +
		    C_BLACKLISTED + T_BOOLEAN + T_DEFAULT + T_NULL +
		    "," +
		    C_IMAGE + T_BLOB +
		    "," +
		    C_WHITELISTED + T_BOOLEAN + T_NOT_NULL + T_DEFAULT + " 0 " + 
		    T_PRIMARY_KEY + "(" + C_COUNTRY_NAME + ASC + ")" +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Currency_Symbols{
    	public static final String TABLE_NAME = "CURRENCY_SYMBOLS";
    	public static final String C_CURRENCY_CODE = "CURRENCY_CODE";
    	public static final String C_SYMBOL = "SYMBOL";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_CURRENCY_CODE + T_VARCHAR + T_PRIMARY_KEY + T_NOT_NULL +
    	    "," + 
		    C_SYMBOL + T_VARCHAR +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Customer{
    	public static final String TABLE_NAME = "CUSTOMER";
    	public static final String C_EMAIL = "EMAIL";
    	public static final String C_PASSWORD = "PASSWORD";
    	public static final String C_PIN = "PIN";
    	public static final String C_CARD_NUMBER = "CARD_NUMBER";
    	public static final String C_COUNTRY_OF_MOBILE = "COUNTRY_OF_MOBILE";
    	public static final String C_FIRST_NAME = "FIRST_NAME";
    	public static final String C_LAST_NAME = "LAST_NAME";
    	public static final String C_ID = "_ID";
    	public static final String C_MOBILE_PHONE = "MOBILE_PHONE";
    	public static final String C_BALANCE = "BALANCE";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
		    C_EMAIL + T_TEXT +
    	    "," + 
    	    C_PASSWORD + T_TEXT +
    	    "," + 
    	    C_PIN + T_TEXT +
    	    "," + 
    	    C_CARD_NUMBER + T_TEXT +
    	    "," + 
    	    C_COUNTRY_OF_MOBILE + T_TEXT +
    	    "," + 
    	    C_FIRST_NAME + T_TEXT +
    	    "," + 
    	    C_LAST_NAME + T_TEXT +
    	    "," + 
    	    C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
		    C_MOBILE_PHONE + T_TEXT +
		    "," +
		    C_BALANCE + T_FLOAT +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Taxes{
    	public static final String TABLE_NAME = "TAXES";
    	public static final String C_ID = "_ID";
    	public static final String C_IS_ENABLE = "IS_ENABLE";
    	public static final String C_COUNTRY = "COUNTRY";
    	public static final String C_PROVINCE = "PROVINCE";
    	public static final String C_RATE = "RATE";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_IS_ENABLE + T_INTEGER + T_DEFAULT + " 0 " + 
    	    "," + 
    	    C_COUNTRY + T_TEXT +
    	    "," + 
    	    C_PROVINCE + T_TEXT +
    	    "," + 
    	    C_RATE + T_REAL +
    	    "," + 
    	    C_CUSTOMER_ID + T_INTEGER +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    public static final class Tips{
    	public static final String TABLE_NAME = "TIPS";
    	public static final String C_ID = "_ID";
    	public static final String C_IS_ENABLE = "IS_ENABLE";
    	public static final String C_DEFAULT_TYPE = "DEFAULT_TYPE";
    	public static final String C_DEFAULT_VALUE = "DEFAULT_VALUE";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_IS_ENABLE + T_INTEGER + T_DEFAULT + " 0 " + 
    	    "," + 
    	    C_DEFAULT_TYPE + T_INTEGER + T_DEFAULT + " 0 " +
    	    "," + 
    	    C_DEFAULT_VALUE + T_REAL + T_DEFAULT + " 0 " +
    	    "," + 
    	    C_CUSTOMER_ID + T_INTEGER +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    
    public static final class Taxes_Param{
    	public static final String TABLE_NAME = "TAXES_PARAM";
    	public static final String C_ID = "_ID";
    	public static final String C_COUNTRY = "COUNTRY";
    	public static final String C_PROVINCE = "PROVINCE";
    	public static final String C_RATE = "RATE";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_COUNTRY + T_VARCHAR +  
    	    "," + 
    	    C_PROVINCE + T_VARCHAR +
    	    "," + 
    	    C_RATE + T_INTEGER +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }

    public static final class Transaction_History{
    	public static final String TABLE_NAME = "TRANSACTION_HISTORY";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	public static final String C_ORDER_ID = "ORDER_ID";
    	public static final String C_TRANSACTION_DATE = "TRANSACTION_DATE";
    	public static final String C_PAYMENT_METHOD = "PAYMENT_METHOD";
    	public static final String C_PRODUCT_ID = "PRODUCT_ID";
    	public static final String C_QTY = "QTY";
    	public static final String C_TOTAL_AMOUNT = "TOTAL_AMOUNT";
    	public static final String C_STATUS = "STATUS";
    	public static final String C_TIP_VALUE = "TIP_VALUE";
    	public static final String C_TAXES_RATE = "TAXES_RATE";
    	public static final String C_SUBTOTAL = "SUBTOTAL";
    	
    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_ORDER_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_CUSTOMER_ID + T_INTEGER +  
    	    "," + 
    	    C_TRANSACTION_DATE + T_LONG +
    	    "," + 
    	    C_PAYMENT_METHOD + T_VARCHAR +
    	    "," + 
    	    C_PRODUCT_ID + T_INTEGER +
    	    "," + 
    	    C_TIP_VALUE + T_REAL +
    	    "," + 
    	    C_TAXES_RATE + T_REAL +
    	    "," + 
    	    C_SUBTOTAL + T_FLOAT +
    	    "," + 
    	    C_QTY + T_INTEGER +
    	    "," + 
    	    C_TOTAL_AMOUNT + T_FLOAT +
    	    "," + 
    	    C_STATUS + T_BOOLEAN +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    
    public static final class ProductDB{
    	public static final String TABLE_NAME = "PRODUCT";
    	public static final String C_PRODUCT_ID = "PRODUCT_ID";
    	public static final String C_PRODUCT_NAME = "PRODUCT_NAME";
    	public static final String C_PRODUCT_DESC = "PRODUCT_DESC";
    	public static final String C_AMOUNT = "AMOUNT";
    	public static final String C_PRODUCT_IMG = "PRODUCT_IMG";

    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_PRODUCT_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_PRODUCT_NAME + T_VARCHAR +  
    	    "," + 
    	    C_PRODUCT_DESC + T_VARCHAR +
    	    "," + 
    	    C_AMOUNT + T_FLOAT +
    	    "," + 
    	    C_PRODUCT_ID + T_INTEGER +
    	    "," + 
    	    C_PRODUCT_IMG + T_BLOB +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    
    public static final class Card_Info{
    	public static final String TABLE_NAME = "CARD_INFO";
    	public static final String C_ID = "ID";
    	public static final String C_CUSTOMER_ID = "CUSTOMER_ID";
    	public static final String C_CARD_NAME = "CARD_NAME";
    	public static final String C_CARD_NUMBER = "CARD_NUMBER";
    	public static final String C_CARD_TYPE = "CARD_TYPE";

    	public static String createTableSql(){
    		return CREATE_TABLE + TABLE_NAME +
    		" (" +
    		C_ID + T_INTEGER + T_PRIMARY_KEY + T_AUTOINCREMENT + 
    	    "," + 
    	    C_CUSTOMER_ID + T_INTEGER +  
    	    "," + 
    	    C_CARD_NAME + T_VARCHAR +
    	    "," + 
    	    C_CARD_NUMBER + T_VARCHAR +
    	    "," + 
    	    C_CARD_TYPE + T_VARCHAR +
		    ");";
    	}
    	public static String updateTableSql(){
    		return "DROP TABLE IF EXISTS " + TABLE_NAME;
    	}
    }
    
}
