package com.drv.beans;

import java.util.ArrayList;

public class CustomerInfo {
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCountryOfMobile() {
		return countryOfMobile;
	}
	public void setCountryOfMobile(String countryOfMobile) {
		this.countryOfMobile = countryOfMobile;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}

	public ArrayList<CardInfo> getCardInfos() {
		return cardInfos;
	}
	public void setCardInfos(ArrayList<CardInfo> cardInfos) {
		this.cardInfos = cardInfos;
	}
	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}
	public Taxes getTaxes() {
		return taxes;
	}
	public void setTaxes(Taxes taxes) {
		this.taxes = taxes;
	}
	public Tips getTips() {
		return tips;
	}
	public void setTips(Tips tips) {
		this.tips = tips;
	}

	private float balance;
	private String email;
	private String password;
	private String pin;
	private String cardNumber;
	private String countryOfMobile;
	private String firstname;
	private String lastname;
	private String mobilePhone; 
	private long id;
	
	private ArrayList<CardInfo> cardInfos;
	private ArrayList<Transaction>  transactions;
	private Taxes taxes;
	private Tips tips;
}
