package com.drv.beans;

import java.io.Serializable;
import java.util.Date;

public class Transaction implements Serializable, Comparable<Transaction> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String transactionName;
	private String transactionDesc;
	private float totalAmount;
	private Date transactionDate;
	private String paymentMethod;
	private int orderId;
	private int productId;
	private int qty;
	private boolean status;
	private float tipValue;
	private float taxValue;
	private float subTotal;

	private double xAxis;
	private String currency = "$";
	private float amount;

	public float getTipValue() {
		return tipValue;
	}

	public void setTipValue(float tipValue) {
		this.tipValue = tipValue;
	}

	public float getTaxValue() {
		return taxValue;
	}

	public void setTaxValue(float taxValue) {
		this.taxValue = taxValue;
	}

	public float getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(float subTotal) {
		this.subTotal = subTotal;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public double getxAxis() {
		return xAxis;
	}

	public void setxAxis(double xAxis) {
		this.xAxis = xAxis;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getTransactionDesc() {
		return transactionDesc;
	}

	public void setTransactionDesc(String transactionDesc) {
		this.transactionDesc = transactionDesc;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public float getTotalBalance() {
		return totalAmount;
	}

	public void setTotalBalance(float totalBalance) {
		this.totalAmount = totalBalance;
	}

	@Override
	public int compareTo(Transaction another) {
		// TODO Auto-generated method stub
		return getTransactionDate().compareTo(another.getTransactionDate());
	}
}
