package com.drv.mpos;

import android.app.ActionBar;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class AboutUsActivity extends BaseActivity {
	
	LocalActivityManager mlam;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us);
		
		mlam = new LocalActivityManager(this, false);
        mlam.dispatchCreate(savedInstanceState);
        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(mlam);
 
		// Donriver tab        
        ImageView donriverLogoView = new ImageView(this);
        donriverLogoView.setImageDrawable(getResources().getDrawable(R.drawable.donriverlogo));
        
		Intent intentDR = new Intent().setClass(this, AboutUsDonriverActivity.class);
		TabSpec tabSpecDR = tabHost
		  .newTabSpec("Donriver")
		  .setIndicator(donriverLogoView)
		  .setContent(intentDR);
 
		// Securekey tab		
		ImageView secureLogoView = new ImageView(this);
		secureLogoView.setImageDrawable(getResources().getDrawable(R.drawable.securekeylogo_large));
		
		Intent intentSK = new Intent().setClass(this, AboutUsSecurekeyActivity.class);
		TabSpec tabSpecSK = tabHost
		  .newTabSpec("SecureKey")
		  .setIndicator(secureLogoView)
		  .setContent(intentSK);
		
		tabHost.addTab(tabSpecDR);
		tabHost.addTab(tabSpecSK);
		
		tabHost.setCurrentTab(0);
	}
}
