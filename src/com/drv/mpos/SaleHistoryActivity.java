package com.drv.mpos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.drv.adapter.TransactionHistoryAdapter;
import com.drv.beans.Transaction;
import com.drv.common.Utils;

public class SaleHistoryActivity extends BaseActivity {

	private ExpandableListView expList;
	private HashMap<String, List<Transaction>> transactionHashMap;
	TransactionHistoryAdapter adapter;
	private ArrayList<String> keys;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.sale_history);
			this.groupDataByTransactionList(Utils.transactionList);

			expList = (ExpandableListView) findViewById(R.id.historyList);
			expList.setGroupIndicator(null);
			adapter = new TransactionHistoryAdapter(this, keys, transactionHashMap);
			expList.setAdapter(adapter);
			expList.setOnChildClickListener(new OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					Log.d("aaaaa", "select item " + childPosition + " in group"
							+ groupPosition);
					Intent intent = new Intent();
					intent.setClass(SaleHistoryActivity.this,
							SaleDetailsActivity.class);
					intent.putExtra("TransactionItem",
							adapter.getChild(groupPosition, childPosition));
					startActivity(intent);
					return true;
				}
			});

		} catch (Exception e) {
			Log.d("Error", "Errrr +++ " + e.getMessage());
		}
	}

	private void groupDataByTransactionList(List<Transaction> transactionList) {
		List<Transaction> tmpList = new ArrayList<Transaction>();
		keys = new ArrayList<String>();
		Collections.sort(transactionList);
		
		tmpList.add(transactionList.get(0));
		transactionHashMap = new HashMap<String, List<Transaction>>();
		
		String date = Utils.convertDateToString(transactionList.get(0)
				.getTransactionDate(), "MMMM dd, yyyy");
		transactionHashMap.put(date, tmpList);
		keys.add(date);

		for (int i = 1; i < transactionList.size(); i++) {
			String text = Utils.convertDateToString(transactionList.get(i)
					.getTransactionDate(), "MMMM dd, yyyy");
			if (transactionHashMap.containsKey(text)) {
				List<Transaction> tmpList1 = transactionHashMap.get(text);
				tmpList1.add(transactionList.get(i));
			} else {
				// System.out.println("date not exist:" + text);
				List<Transaction> tmpList2 = new ArrayList<Transaction>();
				tmpList2.add(transactionList.get(i));
				transactionHashMap.put(text, tmpList2);
				keys.add(text);
			}
		}
	}
}