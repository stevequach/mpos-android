package com.drv.mpos;

import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.CustomerInfo;
import com.drv.common.Utils;
import com.drv.database.DatabaseManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AnonymousBaseActivity implements OnClickListener {

	private CustomNormalFontTextView forgotPasswordLink;
	private CustomNormalFontTextView registerLink;
	private EditText emailEditText;
	private EditText passwordEditText;
	private Button loginButton;
	private Button tapInButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.login);

		emailEditText = (EditText) findViewById(R.id.editTextEmail);
		passwordEditText = (EditText) findViewById(R.id.editTextPassword);
		forgotPasswordLink = (CustomNormalFontTextView) findViewById(R.id.textviewForgotPassword);
		registerLink = (CustomNormalFontTextView) findViewById(R.id.textviewRegister);
		loginButton = (Button) findViewById(R.id.buttonLogin);
		tapInButton = (Button) findViewById(R.id.buttonTapIn);
		// if(isVisible){
		// tapInButton.setVisibility(View.VISIBLE);
		// }

		forgotPasswordLink.setOnClickListener(this);
		registerLink.setOnClickListener(this);
		loginButton.setOnClickListener(this);
		tapInButton.setOnClickListener(this);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == registerLink) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, RegisterActivity.class);
			startActivity(intent);
		} else if (v == forgotPasswordLink) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, ForgotPasswordActivity.class);
			startActivity(intent);
		} else if (v == tapInButton) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, TapACardActivity.class);
			intent.putExtra("TapACardPreviousScreen", "LoginScreen");
			startActivity(intent);
		} else if (v == loginButton) {
			// validate input fields
			String inputError = this.validateInputField();
			// no error
			if (inputError == null) {
				DatabaseManager dbman = DatabaseManager.getInstance(LoginActivity.this);
				CustomerInfo ci = dbman.customerByEmail(emailEditText.getText().toString());
				if(ci == null){//email does not exist in system
					Toast.makeText(getApplicationContext(), "The email you enter does not exist on system. Please try again!",
							Toast.LENGTH_SHORT).show();
				}
				else{//email exist
					if(passwordEditText.getText().toString().equals(ci.getPassword())){//login success
						// if it's the first login, navigated to AssociateContactlessCardActivity
						// else, navigated to DashboardActivity
//				if (firstLogin == true) {
						
						Utils.customerInfo = ci;
						
						Intent intent = new Intent();
						intent.setClass(LoginActivity.this, AssociateContactlessCardActivity.class);
						startActivity(intent);
//				} else {
//					Intent intent = new Intent();
//					intent.setClass(LoginActivity.this, DashboardActivity.class);
//					startActivity(intent);
//				}				
					}
					else{//wrong password
						Toast.makeText(getApplicationContext(), "The login credential is not correct. Please try again!",
								Toast.LENGTH_SHORT).show();
					}
					
				}
			} else { // input error
				Toast.makeText(getApplicationContext(), inputError,
						Toast.LENGTH_SHORT).show();
			}
		}else{
//			super.onClick(v);
		}
	}

	protected String validateInputField() {
		if (emailEditText.getText().toString().length() == 0) {
			return getString(R.string.login_empty_email);
		}
		if (passwordEditText.getText().toString().length() == 0) {
			return getString(R.string.login_empty_password);
		}
		return null;

	}
}
