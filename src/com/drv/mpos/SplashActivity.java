package com.drv.mpos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

public class SplashActivity extends Activity{

	private Thread splashThread;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		splashThread = new Thread(){

			public void run() {
				// TODO Auto-generated method stub
				try {
                    synchronized(this){
                        // Wait given period of time or exit on touch
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){                    
                }
				Intent intent = new Intent();
		        intent.setClass(SplashActivity.this, LoginActivity.class);
		        startActivity(intent);
                finish();
			}
			
		};
		splashThread.start();
	}

    public boolean onTouchEvent(MotionEvent evt)
    {
        if(evt.getAction() == MotionEvent.ACTION_DOWN)
        {
            synchronized(splashThread){
                splashThread.notifyAll();
            }
        }
        return true;
    } 
	
}
