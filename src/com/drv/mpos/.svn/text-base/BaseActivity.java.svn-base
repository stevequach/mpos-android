package com.drv.mpos;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class BaseActivity extends Activity implements AnimationListener,
		OnClickListener {
	private LinearLayout menu;
	private ImageButton menuImageButton;

	private ImageButton dashboardImageButton;
	private ImageButton newSaleImageButton;
	private ImageButton productCatalogImageButton;
	private ImageButton saleHistoryImageButton;
	private ImageButton userProfileImageButton;
	private ImageButton tillManagementImageButton;
	private ImageButton aboutUsImageButton;
	private ImageButton helpImageButton;
	private boolean menuOut = false;
	private BaseActivity baseActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		baseActivity = this;

		ActionBar action = getActionBar();
		action.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.titlebar768x1280));

		action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		action.setCustomView(R.layout.action_bar);
		action.setDisplayShowCustomEnabled(true);
		action.setDisplayShowHomeEnabled(false);
		action.setDisplayShowTitleEnabled(false);

		View customeView = action.getCustomView();
		menuImageButton = (ImageButton) customeView
				.findViewById(R.id.menuImageButton);
		menuImageButton.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (menu == null) {
			LayoutInflater inflater = LayoutInflater.from(BaseActivity.this);
			View view = inflater.inflate(R.layout.main_menu, null);
			LayoutParams params = new LayoutParams(
					FrameLayout.LayoutParams.MATCH_PARENT,
					FrameLayout.LayoutParams.MATCH_PARENT);
			this.addContentView(view, params);

			FrameLayout menuFrameLayout = (FrameLayout) view
					.findViewById(R.id.menuFrameLayout);
			menu = (LinearLayout) menuFrameLayout
					.findViewById(R.id.menuLinearLayout);

			this.imageButtonOnClick();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list_options_menu, menu);

		Intent intent = new Intent(null, getIntent().getData());
		intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
		menu.addIntentOptions(Menu.CATEGORY_ALTERNATIVE, 0, 0,
				new ComponentName(this, BaseActivity.class), null, intent, 0,
				null);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_new_sale: {
			Intent intent = new Intent();
			intent.setClass(baseActivity, SelectProductActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.menu_tips: {
			Intent intent = new Intent();
			intent.setClass(baseActivity, TipsActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.menu_taxes: {
			Intent intent = new Intent();
			intent.setClass(baseActivity, TaxesActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.menu_bussiness_profile: {
			Intent intent = new Intent();
			intent.setClass(baseActivity, BusinessProfileActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.menu_bank_account: {
			Intent intent = new Intent();
			intent.setClass(baseActivity, BankAccountActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.menu_signout: {
			Intent intent = new Intent();
			intent.setClass(BaseActivity.this, LoginActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showLeftMenu() {
		Context context = baseActivity;
		Animation anim;
		if (!menuOut) {
			menu.setVisibility(View.VISIBLE);
			anim = AnimationUtils.loadAnimation(context, R.anim.push_right_in);
		} else {
			anim = AnimationUtils.loadAnimation(context, R.anim.push_left_out);
		}
		anim.setAnimationListener(baseActivity);
		menu.startAnimation(anim);
	}

	private void hideLeftMenu() {
		if (menuOut) {
			Context context = baseActivity;
			Animation anim;
			anim = AnimationUtils.loadAnimation(context, R.anim.push_left_out);
			anim.setAnimationListener(baseActivity);
			menu.startAnimation(anim);
		}
	}

	@Override
	public void onAnimationEnd(Animation arg0) {
		menuOut = !menuOut;
		if (!menuOut) {
			menu.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
	}

	@Override
	public void onAnimationStart(Animation arg0) {
	}

	private void imageButtonOnClick() {
		// Dashboard
		dashboardImageButton = (ImageButton) menu
				.findViewById(R.id.dashboardButton);
		dashboardImageButton.setOnClickListener(this);

		// New Sale
		newSaleImageButton = (ImageButton) menu
				.findViewById(R.id.newSaleButton);
		newSaleImageButton.setOnClickListener(this);

		// Product Catalog
		productCatalogImageButton = (ImageButton) menu
				.findViewById(R.id.productCatalogButton);
		productCatalogImageButton.setOnClickListener(this);

		// Sale History
		saleHistoryImageButton = (ImageButton) menu
				.findViewById(R.id.saleHistoryButton);
		saleHistoryImageButton.setOnClickListener(this);

		// User Profile
		userProfileImageButton = (ImageButton) menu
				.findViewById(R.id.userProfileButton);
		userProfileImageButton.setOnClickListener(this);

		// Till Management
		tillManagementImageButton = (ImageButton) menu
				.findViewById(R.id.tillManagementButton);
		tillManagementImageButton.setOnClickListener(this);

		// About Us
		aboutUsImageButton = (ImageButton) menu
				.findViewById(R.id.aboutUsButton);
		aboutUsImageButton.setOnClickListener(this);

		// Help
		helpImageButton = (ImageButton) menu.findViewById(R.id.helpButton);
		helpImageButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == menuImageButton) {
			if (getCurrentFocus() != null) {
				InputMethodManager ipm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
				ipm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);				
			}
			this.showLeftMenu();
		} else {
			if (v == dashboardImageButton) {

				Intent intent = new Intent();
				intent.setClass(baseActivity, DashboardActivity.class);
				startActivity(intent);
			} else if (v == newSaleImageButton) {
				Intent intent = new Intent();
				intent.setClass(baseActivity, SelectProductActivity.class);
				startActivity(intent);
			} else if (v == productCatalogImageButton) {
				// Intent intent = new Intent();
				// intent.setClass(baseActivity, ProductCatalogActivity.class);
				// startActivity(intent);
			} else if (v == saleHistoryImageButton) {
				Intent intent = new Intent();
				intent.setClass(baseActivity, SaleHistoryActivity.class);
				startActivity(intent);
			} else if (v == userProfileImageButton) {
				Intent intent = new Intent();
				intent.setClass(baseActivity, UserProfileActivity.class);
				startActivity(intent);
			} else if (v == tillManagementImageButton) {
				// Intent intent = new Intent();
				// intent.setClass(baseActivity, TillManagementActivity.class);
				// startActivity(intent);
			} else if (v == aboutUsImageButton) {
				Intent intent = new Intent();
				intent.setClass(baseActivity, AboutUsActivity.class);
				startActivity(intent);
			} else if (v == helpImageButton) {
				// Intent intent = new Intent();
				// intent.setClass(baseActivity, HelpActivity.class);
				// startActivity(intent);
			}
			this.hideLeftMenu();
		}
	}
}
