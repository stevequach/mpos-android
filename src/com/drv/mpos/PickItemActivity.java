package com.drv.mpos;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.drv.adapter.CustomListViewAdapter;
import com.drv.beans.PickItem;
import com.drv.beans.Product;
import com.drv.common.Utils;
import com.drv.database.DatabaseManager;

public class PickItemActivity extends BaseActivity implements
		OnItemClickListener {

	ListView listView;
	List<PickItem> rowItems;
	List<Product> productList;

    
	/*private void createProductSampleDate(){
		productList = new ArrayList<Product>();
		String productDesc = "Product Description";

		Product product = new Product();
		product.setProductName("Service Product 1");
		product.setIconName(R.drawable.dogwalker);
		product.setProductDesc(productDesc);
		product.setAmount(2500);
		productList.add(product);

		Product product1 = new Product();
		product1.setProductName("Service Product 2");
		product1.setIconName(R.drawable.golfstuff);
		product1.setProductDesc(productDesc);
		product1.setAmount(10);
		productList.add(product1);

		Product product2 = new Product();
		product2.setProductName("Service Product 3");
		product2.setIconName(R.drawable.hamburgerimage);
		product2.setProductDesc(productDesc);
		product2.setAmount(775);
		productList.add(product2);

		Product product3 = new Product();
		product3.setProductName("Goods Product 1");
		product3.setIconName(R.drawable.manicure);
		product3.setProductDesc(productDesc);
		product3.setAmount(4999);
		productList.add(product3);

		Product product4 = new Product();
		product4.setProductName("Goods Product 2");
		product4.setIconName(R.drawable.tennisshoes);
		product4.setProductDesc(productDesc);
		product4.setAmount(2495);
		productList.add(product4);

		Product product5 = new Product();
		product5.setProductName("Goods Product 3");
		product5.setIconName(R.drawable.tie);
		product5.setProductDesc(productDesc);
		product5.setAmount(198);
		productList.add(product5);
		
		Product product6 = new Product();
		product6.setProductName("Goods Product 4");
		product6.setIconName(R.drawable.hamburgerimage);
		product6.setProductDesc(productDesc);
		product6.setAmount(587);
		productList.add(product6);
		
		Product product7 = new Product();
		product7.setProductName("Goods Product 5");
		product7.setIconName(R.drawable.tennisshoes);
		product7.setProductDesc(productDesc);
		product7.setAmount(6215);
		productList.add(product7);
	}*/

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pick_item);

		DatabaseManager dbman = DatabaseManager.getInstance(PickItemActivity.this);
		productList = new ArrayList<Product>();
		productList = dbman.getAllProduct();

		rowItems = new ArrayList<PickItem>();
		for (int i = 0; i < productList.size(); i++) {

			String productName = productList.get(i).getProductName();
			String productDesc = productList.get(i).getProductDesc();
			float amount = productList.get(i).getAmount() / 100;			
			byte[] productImage = productList.get(i).getProductImage();
			

			PickItem item = new PickItem(productImage, productName, productDesc,
					"$" + Utils.formatAmountToString(amount));
			rowItems.add(item);
		}

		listView = (ListView) findViewById(R.id.pickItemList);
		CustomListViewAdapter adapter = new CustomListViewAdapter(this,
				R.layout.list_item, rowItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Product productSelected = productList.get(position);

		Intent intent = new Intent();
		intent.putExtra("Product", productSelected);
		intent.setClass(PickItemActivity.this, SelectPaymentActivity.class);
		startActivity(intent);
	}
}
