package com.drv.mpos;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class AboutUsSecurekeyActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us_securekey);
		
		String secureKeyHtml = "<Font color=\"#68a5d9\" size=\"5\"><I><B>About Secure Key:<B></I></Font> <br/><br/>" +
        		"SecureKey provides an innovative, cloud-based authentication service" +
        		" that enhances privacy and security for industries including government, " +
        		"financial services, healthcare, and telecoms. By embedding its security application" +
        		" in laptops, tablets, and mobile devices, SecureKey turns ordinary consumer devices " +
        		"into secure terminals for multifactor and federated authentication, using the security " +
        		"of chip -based identity and payment credentials. Highly scalable and consumer-friendly, " +
        		"SecureKey enhances a wide range of online, mobile, and in-person identity and payment applications." +
        		" SecureKey is a Privacy by Design (PbD) Ambassador based in Toronto, Canada, " +
        		"with backing from leading technology, payments and mobile industry network providers. " +
        		"For more information, visit <a href=\"http://www.securekey.com/\">www.securekey.com</a>";
		
		WebView wv = (WebView)findViewById(R.id.webView1);
		wv.loadData(secureKeyHtml, "text/html", null);
	}
}
