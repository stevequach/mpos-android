package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ContactInformationActivity extends BaseActivity {

	private EditText emailEditText;
	private EditText countryOfMoblieEditText;
	private EditText phoneNumberEditText;
	private TextView countryCodeTextView;
	private Button saveButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_information);

		emailEditText = (EditText) findViewById(R.id.editTextEmail);
		countryOfMoblieEditText = (EditText)findViewById(R.id.editTextCountryOfMobile);
		countryOfMoblieEditText.setOnKeyListener(null);
		countryOfMoblieEditText.setFocusable(false);
		countryOfMoblieEditText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ContactInformationActivity.this, SelectCountryActivity.class);
				startActivityForResult(intent, 1);
			}
		});
		phoneNumberEditText = (EditText) findViewById(R.id.editTextPhoneNumber);
		countryCodeTextView = (TextView) findViewById(R.id.textViewCountryCode);
		saveButton = (Button) findViewById(R.id.buttonSave);
		saveButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		super.onClick(view);
		if (view == saveButton) {
			finish();
		}

	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			switch (requestCode) {
			case 1:
			{
				countryOfMoblieEditText.setText(data.getStringExtra("countryname"));
				countryCodeTextView.setText("(+"+data.getStringExtra("dialing") + ")");
			}
				break;

			default:
				break;
			}
		}
	}
}
