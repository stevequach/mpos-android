package com.drv.mpos;

import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.drv.adapter.CustomBoldFontTextView;
import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.Transaction;
import com.drv.common.Utils;

public class SaleDetailsActivity extends BaseActivity {
	private CustomNormalFontTextView orderNumberTextView;
	private CustomNormalFontTextView dateTextView;
	private CustomNormalFontTextView timeTextView;
	private CustomNormalFontTextView statusTextView;
	private CustomNormalFontTextView methodTextView;
	private CustomNormalFontTextView subTotalTextView;
	private CustomNormalFontTextView taxTextView;
	private CustomBoldFontTextView totalTextView;
	private EditText emailEditText;
	private EditText phoneNumberEditText;

	private Transaction transactionItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sale_details);
		orderNumberTextView = (CustomNormalFontTextView) findViewById(R.id.detailsOrderNumberTextView);
		dateTextView = (CustomNormalFontTextView) findViewById(R.id.detailsDateTextView);
		timeTextView = (CustomNormalFontTextView) findViewById(R.id.detailsTimeTextView);
		statusTextView = (CustomNormalFontTextView) findViewById(R.id.detailsStatusTextView);
		methodTextView = (CustomNormalFontTextView) findViewById(R.id.detailsMethodTextView);
		subTotalTextView = (CustomNormalFontTextView) findViewById(R.id.detailsSubTotalTextView);
		taxTextView = (CustomNormalFontTextView) findViewById(R.id.detailsTaxTextView);
		totalTextView = (CustomBoldFontTextView) findViewById(R.id.detailsTotalTextView);
		emailEditText = (EditText) findViewById(R.id.detailsEmailEditText);
		phoneNumberEditText = (EditText) findViewById(R.id.detailsMobilePhoneEditText);

		Intent intent = getIntent();
		transactionItem = (Transaction) intent
				.getSerializableExtra("TransactionItem");

		Date transactionDate = transactionItem.getTransactionDate();
		dateTextView.setText(Utils.convertDateToString(transactionDate,
				"yyyy-MM-dd"));
		timeTextView.setTag(Utils.convertDateToString(transactionDate,
				"HH:mm aa"));

		subTotalTextView.setText(Utils.formatAmountToString(transactionItem.getAmount()));
		taxTextView.setText(Utils.formatAmountToString(transactionItem.getTaxValue()));
//		tip.setText(Utils.formatAmountToString(transactionItem.getTaxValue()));
		totalTextView.setText(Utils.formatAmountToString(transactionItem.getTotalAmount()));
	}

	public void sendReceiptOnClick(View v) {
		finish();
	}
}