package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.drv.beans.CustomerInfo;
import com.drv.common.Utils;
import com.drv.database.DatabaseManager;

public class PersonalInformationActivity extends BaseActivity {

	private EditText firstNameEditText;
	private EditText lastNameEditText;
	private Button saveButton;
	private CustomerInfo customerInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.personal_information);
		firstNameEditText = (EditText) findViewById(R.id.editTextFirstName);
		lastNameEditText = (EditText) findViewById(R.id.editTextLastName);
		saveButton = (Button) findViewById(R.id.buttonSave);
		saveButton.setOnClickListener(this);

		//this.loadPersonalInformation();
	}

	private void loadPersonalInformation() {
		customerInfo = Utils.customerInfo;
		if (customerInfo != null) {
			firstNameEditText.setText(customerInfo.getFirstname());
			lastNameEditText.setText(customerInfo.getLastname());
		}
	}

	private void updatePersonalInformation() {
		if (customerInfo == null) {
			customerInfo = new CustomerInfo();
		}
		customerInfo.setFirstname(firstNameEditText.getText().toString());
		customerInfo.setLastname(lastNameEditText.getText().toString());

		DatabaseManager manager = DatabaseManager
				.getInstance(PersonalInformationActivity.this);
		if (manager.updatePersonalInformation(customerInfo)) {
			Utils.customerInfo = customerInfo;
			Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();
			this.finish();
		} else {
			Toast.makeText(this, "Updated Error", Toast.LENGTH_SHORT).show();
		}
	}

	protected String validateInputFields() {
		if (firstNameEditText.getText().toString().isEmpty()) {
			return getString(R.string.register_empty_firstname);
		}
		if (lastNameEditText.getText().toString().isEmpty()) {
			return getString(R.string.register_empty_lastname);
		}
		return null;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		String inputError = validateInputFields();
		// no error
		if (inputError == null) {
			updatePersonalInformation();
		} else {// error
			Toast.makeText(this, inputError, Toast.LENGTH_SHORT).show();
		}
	}
}
