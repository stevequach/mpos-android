package com.drv.mpos;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class AnonymousBaseActivity extends Activity implements OnClickListener{
//	public Context self;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		ActionBar action = getActionBar();
		action.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.titlebar768x1280));
		action.setDisplayShowHomeEnabled(false);
		action.setDisplayShowTitleEnabled(false);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.add(R.string.setting_setting);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent();
			intent.setClass(AnonymousBaseActivity.this, SettingsActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
		return true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
}
