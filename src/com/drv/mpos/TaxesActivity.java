package com.drv.mpos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.drv.beans.Taxes;
import com.drv.beans.TaxesParam;
import com.drv.common.Utils;
import com.drv.database.DatabaseManager;

public class TaxesActivity extends BaseActivity implements OnItemSelectedListener
{	
	private ImageView switchButton;
	private Spinner countrySpinner;
	private Spinner provinceSpinner;
	private Spinner taxRateSpinner;
	private Button saveButton;
	
	private boolean onTaxes = true;
	private HashMap<String, List<TaxesParam>> map;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.taxes);
		
		saveButton = (Button)findViewById(R.id.saveButton);
		saveButton.setOnClickListener(this);
		
		switchButton = (ImageView)findViewById(R.id.switchButton);
		switchButton.setOnClickListener(this);
		
		//get taxes param from db
		DatabaseManager dbman = DatabaseManager.getInstance(TaxesActivity.this);
		List<TaxesParam> taxesParamList = new ArrayList<TaxesParam>();
		taxesParamList = dbman.getTaxesParam();
		
		map = groupTaxesByCountry(taxesParamList);
		
		countrySpinner = (Spinner)findViewById(R.id.countrySpinner);
		countrySpinner.setOnItemSelectedListener(this);
		provinceSpinner = (Spinner)findViewById(R.id.provinceSpinner);
		provinceSpinner.setOnItemSelectedListener(this);
		taxRateSpinner = (Spinner)findViewById(R.id.taxRateSpinner);
		taxRateSpinner.setOnItemSelectedListener(this);
		
		String[] countries = new String[map.size()];
		int i=0;
		for (Map.Entry<String,List<TaxesParam>> entry : map.entrySet()) {
			  String key = entry.getKey();
			  countries[i]= key;
			  i++;
		}
		
		String[] provinces = getProvinceByCountry(countries[0]);
		List<String> rates = getRateByProvince(countries[0], provinces[0]);
		
		ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);
		countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countrySpinner.setAdapter(countryAdapter);		
		
		ArrayAdapter<String> provinceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, provinces);
		provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		provinceSpinner.setAdapter(provinceAdapter);
		
		ArrayAdapter<String> rateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, rates);
		rateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		taxRateSpinner.setAdapter(rateAdapter);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	  if (v == switchButton){
			if (onTaxes){
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.offswitch));
				countrySpinner.setEnabled(false);
				provinceSpinner.setEnabled(false);
				taxRateSpinner.setEnabled(false);
				onTaxes = false;
			}
			else{
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.onswitch));
				countrySpinner.setEnabled(true);
				provinceSpinner.setEnabled(true);
				taxRateSpinner.setEnabled(true);
				onTaxes = true;
			}
		}
		else if(v == saveButton){
//			finish();			
			if(onTaxes){
				Taxes taxes = new Taxes();
				taxes.setCountry(countrySpinner.getSelectedItem().toString());
				taxes.setProvince(provinceSpinner.getSelectedItem().toString());
				taxes.setRate(Integer.parseInt(taxRateSpinner.getSelectedItem().toString().substring(0, 2)));
				taxes.setEnable(onTaxes);
				//insert or update tax rate
				DatabaseManager dbman = DatabaseManager.getInstance(TaxesActivity.this);
				boolean chkRate = dbman.chkExistTaxesRate(1);
				System.out.println("chkRate:" + chkRate);
				if(chkRate){
					boolean s = dbman.updateTaxesRate(1, taxes);
					System.out.println("update:" + s);
				}
				else{
					boolean s = dbman.addTaxes(taxes, 1);
					System.out.println("add:" + s);
				}
			}
		}
		
		else{
			super.onClick(v);
		}
	}
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, data);
//		if(resultCode == RESULT_OK){
//			switch (requestCode) {
//			case 1:
//			{
//				countryEditText.setText(data.getStringExtra("countryname"));
//			}
//				break;
//
//			default:
//				break;
//			}
//		}
//	}
	
	private HashMap<String, List<TaxesParam>> groupTaxesByCountry(List<TaxesParam> taxesList) {
		List<TaxesParam> tmpList = new ArrayList<TaxesParam>();
		tmpList.add(taxesList.get(0));
		map = new HashMap<String, List<TaxesParam>>();
		map.put(taxesList.get(0).getCountry().trim(),tmpList);

		for (int i = 1; i < taxesList.size(); i++) {
			String text = taxesList.get(i).getCountry().trim();
			if (map.containsKey(text)) {
				List<TaxesParam> tmpList1 = map.get(text);
				tmpList1.add(taxesList.get(i));
			} else {
				// System.out.println("date not exist:" + text);
				List<TaxesParam> tmpList2 = new ArrayList<TaxesParam>();
				tmpList2.add(taxesList.get(i));
				map.put(text, tmpList2);
			}
		}	
		return map;
	}
	
	 public void onItemSelected(AdapterView<?> parent, View view, 
	            int pos, long id) {
	        // An item was selected. You can retrieve the selected item using
		 if(parent == countrySpinner){
			 String country = (String) parent.getItemAtPosition(pos);			 
			 String[] provinces = getProvinceByCountry(country);
			 
			 ArrayAdapter<String> provinceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, provinces);	         
			 provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	         provinceSpinner.setAdapter(provinceAdapter);
		 }
		 else if(parent == provinceSpinner){
	         String province = (String) parent.getItemAtPosition(pos);
	         
	         List<String> taxRates = getRateByProvince(countrySpinner.getItemAtPosition(0).toString(), province);
	        
	         ArrayAdapter<String> taxesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, taxRates);	         
	         taxesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	         taxRateSpinner.setAdapter(taxesAdapter);
		 }
	 }

	 public void onNothingSelected(AdapterView<?> parent) {
	        // Another interface callback
	 }
	 
	 public String[] getProvinceByCountry(String country){
		 List<TaxesParam> list = map.get(country);
		 
		 String[] provinces = new String[list.size()];
		 for (int i = 0; i < list.size(); i++) {
			 String pr = list.get(i).getProvince();
			provinces[i] = pr;
		 }
		 return provinces;
	 }
	 
	 public List<String> getRateByProvince(String country, String province){
		 List<TaxesParam> list = map.get(country);
		 
		 List<String> rates = new ArrayList<String>();
		 for (int i = 0; i < list.size(); i++) {
			 String pr = list.get(i).getProvince();
			 if(pr.equals(province)){
				 int rate = list.get(i).getRate();
				 rates.add(Utils.formatAmountToString(rate));
			 }
		}
		return rates;
	 }
}
