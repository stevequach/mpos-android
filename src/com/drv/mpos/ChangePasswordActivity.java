package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ChangePasswordActivity extends BaseActivity {
	EditText currentPasswordEditText;
	EditText newPasswordEditText;
	EditText confirmPasswordEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password);
		currentPasswordEditText = (EditText) findViewById(R.id.changePasswordCurrentEditText);
		newPasswordEditText = (EditText) findViewById(R.id.changePasswordNewEditText);
		confirmPasswordEditText = (EditText) findViewById(R.id.changePasswordConfirmEditText);
	}

	public void saveOnClick(View v) {
		finish();
	}
}
