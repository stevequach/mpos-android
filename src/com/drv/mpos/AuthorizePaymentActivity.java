package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.drv.adapter.CustomBoldFontTextView;
import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.Transaction;
import com.drv.common.Constant;
import com.drv.common.Utils;

public class AuthorizePaymentActivity extends BaseActivity implements
		OnCheckedChangeListener {
	private CustomNormalFontTextView cardNumberTextView;
	private CustomNormalFontTextView amountTextView;
	private CustomNormalFontTextView tipTextView;
	private CustomBoldFontTextView totalTextView;

	private RadioGroup fixedRadioGroup;
	private RadioGroup percentRadioGroup;
	private RadioButton fixedRadioButton;
	private RadioButton percentRadioButton;
	private TextView numberPickerTextView;
	private ImageView arrowUpImage;
	private ImageView arrowDownImage;

	private Transaction currentTransaction;
	private String currency;
	private float totalAmount;
	private float tip = 0;
	private float totalPay;
	private int percent = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.authorize_payment);

		cardNumberTextView = (CustomNormalFontTextView) findViewById(R.id.authorizePaymentCardNumberTextView);
		amountTextView = (CustomNormalFontTextView) findViewById(R.id.authorizePaymentAmountTextView);
		tipTextView = (CustomNormalFontTextView) findViewById(R.id.authorizePaymentTipTextView);
		totalTextView = (CustomBoldFontTextView) findViewById(R.id.authorizePaymentTotalTextView);

		fixedRadioGroup = (RadioGroup) findViewById(R.id.fixedRadioGroup);
		fixedRadioButton = (RadioButton) findViewById(R.id.fixedRadioButton);
		percentRadioGroup = (RadioGroup) findViewById(R.id.percentRadioGroup);
		percentRadioButton = (RadioButton) findViewById(R.id.percentRadioButton);

		numberPickerTextView = (TextView) findViewById(R.id.numberPickerTextView);
		arrowUpImage = (ImageView) findViewById(R.id.numberPickerUpButton);
		arrowDownImage = (ImageView) findViewById(R.id.numberPickerDownButton);

		numberPickerTextView.setText("10%");
		numberPickerTextView.setTextSize(11);
		arrowDownImage.setOnClickListener(this);
		arrowUpImage.setOnClickListener(this);
		fixedRadioButton.setOnCheckedChangeListener(this);
		percentRadioButton.setOnCheckedChangeListener(this);

		Intent intent = getIntent();
		currentTransaction = (Transaction) intent
				.getSerializableExtra("CurrentTransaction");

		currency = currentTransaction.getCurrency();
		totalAmount = currentTransaction.getTotalAmount();
		totalPay = totalAmount + tip;

		cardNumberTextView.setText(currentTransaction.getTransactionName());
		amountTextView.setText(Utils.formatAmountToString(totalAmount));
		
		this.calculateTotalPay(false);
	}

	public void acceptOnClick(View v) {
		Intent intent = new Intent(AuthorizePaymentActivity.this,
				TransactionCompleteActivity.class);
		currentTransaction.setTotalAmount(totalPay);
		currentTransaction.setTipValue(tip);
		intent.putExtra("CurrentTransaction", currentTransaction);
		this.startActivity(intent);
	}

	public void cancelOnClick(View v) {
		Intent intent = new Intent(AuthorizePaymentActivity.this,
				DashboardActivity.class);
		this.startActivity(intent);
	}

	private void processNumberPickerAction(String type) {
		String value = numberPickerTextView.getText().toString();
		int percent = Integer.parseInt(value.substring(0, value.length() - 1));
		if (type == Constant.UP) {
			if (percent < 50) {
				percent = percent + 5;
			}
		} else {
			if (percent > 10) {
				percent = percent - 5;
			}
		}
		numberPickerTextView.setText(percent + "%");
		this.calculateTotalPay(false);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v == arrowDownImage) {
			processNumberPickerAction(Constant.DOWN);
		} else if (v == arrowUpImage) {
			processNumberPickerAction(Constant.UP);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView == fixedRadioButton) {
			percentRadioGroup.clearCheck();
			this.calculateTotalPay(true);
		} else {
			fixedRadioGroup.clearCheck();
			this.calculateTotalPay(false);
		}
	}

	private void calculateTotalPay(boolean fixedAmount) {
		if (fixedAmount == true) {
			tip = 0;
		} else {
			String value = numberPickerTextView.getText().toString();
			percent = Integer.parseInt(value.substring(0, value.length() - 1));
			tip = totalAmount * percent / 100;
		}
		tipTextView.setText(Utils.formatAmountToString(tip));
		totalPay = totalAmount + tip;
		totalTextView.setText(currency + Utils.formatAmountToString(totalPay));
	}
}
