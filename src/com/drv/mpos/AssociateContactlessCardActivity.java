package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class AssociateContactlessCardActivity extends AnonymousBaseActivity implements OnClickListener{

	private EditText pinEditText;
	private EditText confirmPinEditText;
	private CheckBox enableTapInCheckBox;
	private Button associateButton;
	private TextView skipThisStepLink;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.associate_contactless_card);
		
		pinEditText = (EditText)findViewById(R.id.editTextPin);
		confirmPinEditText = (EditText)findViewById(R.id.editTextConfirmPin);
		enableTapInCheckBox = (CheckBox)findViewById(R.id.checkBoxEnable);
		associateButton = (Button)findViewById(R.id.buttonAssociate);
		skipThisStepLink = (TextView)findViewById(R.id.textViewSkipLink);
		
		associateButton.setOnClickListener(this);
		skipThisStepLink.setOnClickListener(this);
				
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == associateButton){
			Intent intent = new Intent();
			intent.setClass(this, TapACardActivity.class);
			intent.putExtra("TapACardPreviousScreen", "AssociateContactlessCardScreen");
			startActivity(intent);
		}
		else if(v == skipThisStepLink){
			Intent intent = new Intent();
			intent.setClass(this, DashboardActivity.class);
			startActivity(intent);
		}
	}
	protected String validateInputFields() {
		if(enableTapInCheckBox.isChecked()){
			
		}
		if(pinEditText.getText().toString().isEmpty()){
			return getString(R.string.associate_empty_pin);
		}
		if(confirmPinEditText.getText().toString().isEmpty()){
			return getString(R.string.associate_empty_confirm_pin);
		}
		if(pinEditText.getText().toString().equals(confirmPinEditText.getText().toString()) == false){
			return getString(R.string.associate_pin_not_match);
		}
		return null;
	}

}
