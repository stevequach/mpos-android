package com.drv.mpos;

import java.io.ByteArrayInputStream;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.drv.beans.Product;
import com.drv.beans.Transaction;
import com.drv.common.Constant;
import com.drv.common.Utils;

public class SelectPaymentActivity extends BaseActivity {
	private TextView addNotherProductTextView;
	private TextView productNameTextView;
	private TextView productDescTextView;
	private TextView amountTextView;
	private TextView subTotalTextView;
	private TextView taxTextView;
	private TextView totalAmountTextView;
	private TextView numberPickerTextView;
	private ImageView arrowUpImage;
	private ImageView arrowDownImage;
	private ImageView creditCardRadioButtonImage;
	private ImageView cashRadioButtonImage;
	private ImageView chequeRadioButtonImage;
	private ImageView iconImage;
	private Button payButton;

	private float productAmount = 0;
	private float subTotal = 0;
	private float tax = 0;
	private float totalAmount;

	private Product productSelected;
	private String paymentMethod = Constant.CREDIT_CARD;
	private Transaction currentTransaction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_payment);

		productNameTextView = (TextView) findViewById(R.id.productNameTextView);
		productDescTextView = (TextView) findViewById(R.id.productDescTextView);
		amountTextView = (TextView) findViewById(R.id.amountTextView);
		subTotalTextView = (TextView) findViewById(R.id.subtotalTextView);
		totalAmountTextView = (TextView) findViewById(R.id.totalAmountTextView);
		taxTextView = (TextView) findViewById(R.id.taxTextView);
		numberPickerTextView = (TextView) findViewById(R.id.numberPickerTextView);

		arrowUpImage = (ImageView) findViewById(R.id.numberPickerUpButton);
		arrowDownImage = (ImageView) findViewById(R.id.numberPickerDownButton);
		creditCardRadioButtonImage = (ImageView) findViewById(R.id.creditCardRadioButtonImage);
		cashRadioButtonImage = (ImageView) findViewById(R.id.cashCardRadioButtonImage);
		chequeRadioButtonImage = (ImageView) findViewById(R.id.chequeCardRadioButtonImage);
		iconImage = (ImageView) findViewById(R.id.iconImage);
		payButton = (Button) findViewById(R.id.payButton);
		payButton.setOnClickListener(this);
		productSelected = (Product) getIntent().getSerializableExtra("Product");

		productAmount = productSelected.getAmount() / 100;
		float tax = productAmount * (float) Constant.TAX_RATE / 100;
		float totalAmount = productAmount + tax;
		
		if(productSelected.getProductImage() == null){
			iconImage.setImageDrawable(getResources().getDrawable(R.drawable.productimagearea));
		}
		else{
			ByteArrayInputStream imageStream = new ByteArrayInputStream(productSelected.getProductImage());
			Bitmap theImage = BitmapFactory.decodeStream(imageStream);
			iconImage.setImageBitmap(theImage);
		}
		
		productNameTextView.setText(productSelected.getProductName());
		productDescTextView.setText(productSelected.getProductDesc());
		amountTextView.setText("$" + Utils.formatAmountToString(productAmount));

		subTotalTextView.setText(Utils.formatAmountToString(productAmount));
		taxTextView.setText(Utils.formatAmountToString(tax));
		totalAmountTextView.setText("$"
				+ Utils.formatAmountToString(totalAmount));

		addNotherProductTextView = (TextView) findViewById(R.id.addAnotherProductTextView);
		addNotherProductTextView.setOnClickListener(this);

		arrowUpImage.setOnClickListener(this);
		arrowDownImage.setOnClickListener(this);
		creditCardRadioButtonImage.setOnClickListener(this);
		cashRadioButtonImage.setOnClickListener(this);
		chequeRadioButtonImage.setOnClickListener(this);
		
		calculateTotalAmount(1);
	}

	private void calculateTotalAmount(int quantity) {
		subTotal = quantity * productAmount;
		tax = subTotal * (float) Constant.TAX_RATE / 100;
		totalAmount = subTotal + tax;
	}

	private void processNumberPickerAction(String type) {
		int quantity = Integer.parseInt(numberPickerTextView.getText()
				.toString());
		if (type == Constant.UP) {
			quantity++;
		} else {
			if (quantity > 1) {
				quantity--;
			}
		}
		calculateTotalAmount(quantity);
		numberPickerTextView.setText(quantity + "");
		subTotalTextView.setText(Utils.formatAmountToString(subTotal));
		taxTextView.setText(Utils.formatAmountToString(tax));
		totalAmountTextView.setText("$"
				+ Utils.formatAmountToString(totalAmount));
	}

	private void processRadioButton(String type) {
		Drawable selectedImage = getResources().getDrawable(
				R.drawable.radiobutton_selected);
		Drawable unselectedImage = getResources().getDrawable(
				R.drawable.radiobutton_unselected);

		if (type == Constant.CREDIT_CARD) {
			creditCardRadioButtonImage.setImageDrawable(selectedImage);
			cashRadioButtonImage.setImageDrawable(unselectedImage);
			chequeRadioButtonImage.setImageDrawable(unselectedImage);
		} else if (type == Constant.CASH) {
			creditCardRadioButtonImage.setImageDrawable(unselectedImage);
			cashRadioButtonImage.setImageDrawable(selectedImage);
			chequeRadioButtonImage.setImageDrawable(unselectedImage);
		} else {
			creditCardRadioButtonImage.setImageDrawable(unselectedImage);
			cashRadioButtonImage.setImageDrawable(unselectedImage);
			chequeRadioButtonImage.setImageDrawable(selectedImage);
		}
		paymentMethod = type;
	}

	private void initCurrentTransaction() {
		currentTransaction = new Transaction();
		currentTransaction.setTransactionName("********3748");
		currentTransaction.setProductId(productSelected.getProductId());
		currentTransaction.setAmount(productAmount);
		currentTransaction.setSubTotal(subTotal);
		currentTransaction.setTaxValue(tax);
		currentTransaction.setTotalAmount(totalAmount);
		currentTransaction.setPaymentMethod(paymentMethod);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v == arrowUpImage) {
			processNumberPickerAction(Constant.UP);
		} else if (v == arrowDownImage) {
			processNumberPickerAction(Constant.DOWN);
		} else if (v == creditCardRadioButtonImage) {
			processRadioButton(Constant.CREDIT_CARD);
		} else if (v == cashRadioButtonImage) {
			processRadioButton(Constant.CASH);
		} else if (v == chequeRadioButtonImage) {
			processRadioButton(Constant.CHEQUE);
		} else if (v == payButton) {
			this.initCurrentTransaction();
			if (paymentMethod == Constant.CREDIT_CARD) {
				Intent intent = new Intent();
				intent.setClass(SelectPaymentActivity.this,
						AuthorizePaymentActivity.class);
				intent.putExtra("CurrentTransaction", currentTransaction);
				startActivity(intent);
			} else {
				Intent intent = new Intent();
				intent.setClass(SelectPaymentActivity.this,
						AcceptPaymentActivity.class);
				intent.putExtra("CurrentTransaction", currentTransaction);
				startActivity(intent);
			}
		} else if (v == addNotherProductTextView) {
			Intent intent = new Intent();
			intent.setClass(SelectPaymentActivity.this,
					SelectProductActivity.class);
			startActivity(intent);
		}
	}
}
