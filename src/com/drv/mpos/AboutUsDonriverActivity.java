package com.drv.mpos;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class AboutUsDonriverActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us_donriver);
		
		String secureKeyHtml = "<Font color=\"#68a5d9\" size=\"5\"><I><B>About Donriver:<B></I></Font> <br/><br/>";
		
		WebView wv = (WebView)findViewById(R.id.webView1);
		wv.loadData(secureKeyHtml, "text/html", null);
	}

}
