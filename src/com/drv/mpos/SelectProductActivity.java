package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SelectProductActivity extends BaseActivity {

	ImageView pickItemView;
	ImageView manualEntryView;
	ImageView enterBasketView;
	ImageView scanBarcodeView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_product);

		pickItemView = (ImageView) findViewById(R.id.pickItemImg);
		pickItemView.setOnClickListener(this);

		manualEntryView = (ImageView) findViewById(R.id.manualEntryImg);
		manualEntryView.setOnClickListener(this);

		enterBasketView = (ImageView) findViewById(R.id.enterBasketImg);
		enterBasketView.setOnClickListener(this);

		scanBarcodeView = (ImageView) findViewById(R.id.scanBarCodeImg);
		scanBarcodeView.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		if (v == pickItemView) {
			Intent intent = new Intent();
			intent.setClass(SelectProductActivity.this, PickItemActivity.class);
			startActivity(intent);
		}
		if (v == manualEntryView) {
			Intent intent = new Intent();
			intent.setClass(SelectProductActivity.this,
					ManualEntryActivity.class);
			startActivity(intent);
		}
		if(v == scanBarcodeView){
				
		}
	}
}
