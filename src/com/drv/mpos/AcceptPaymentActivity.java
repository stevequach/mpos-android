package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.drv.adapter.CustomBoldFontTextView;
import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.Transaction;
import com.drv.common.Utils;

public class AcceptPaymentActivity extends BaseActivity implements TextWatcher {
	private CustomNormalFontTextView subTotalTextView;
	private CustomNormalFontTextView taxTextView;
	private CustomBoldFontTextView totalTextView;
	private CustomBoldFontTextView changeDueTextView;
	private EditText amountPaidEditText;
	private Transaction currentTransaction;

	private float totalAmount;
	private String currency;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accept_payment);
		subTotalTextView = (CustomNormalFontTextView) findViewById(R.id.acceptPaymentSubTotalTextView);
		taxTextView = (CustomNormalFontTextView) findViewById(R.id.acceptPaymentTaxTextView);
		totalTextView = (CustomBoldFontTextView) findViewById(R.id.acceptPaymentTotalTextView);
		changeDueTextView = (CustomBoldFontTextView) findViewById(R.id.acceptPaymentChangeDueTextView);
		amountPaidEditText = (EditText) findViewById(R.id.acceptPaymentAmountPaidEditText);

		Intent intent = getIntent();
		currentTransaction = (Transaction) intent
				.getSerializableExtra("CurrentTransaction");
		subTotalTextView.setText(Utils.formatAmountToString(currentTransaction
				.getSubTotal()));
		taxTextView.setText(Utils.formatAmountToString(currentTransaction
				.getTaxValue()));

		currency = currentTransaction.getCurrency();
		totalAmount = currentTransaction.getTotalAmount();
		String total = currency + Utils.formatAmountToString(totalAmount);
		totalTextView.setText(total);
		changeDueTextView.setText(currency + "0.00");

		amountPaidEditText.addTextChangedListener(this);
	}

	public void acceptOnClick(View v) {
		Intent intent = new Intent(AcceptPaymentActivity.this,
				TransactionCompleteActivity.class);
		intent.putExtra("CurrentTransaction", currentTransaction);
		this.startActivity(intent);
	}

	public void cancelOnClick(View v) {

	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		float value = Float.parseFloat(amountPaidEditText.getText().toString());
		if (value > totalAmount) {
			changeDueTextView.setText(currency
					+ Utils.formatAmountToString((value - totalAmount)));
		} else {
			changeDueTextView.setText(currency + "0.00");
		}
	}
}
