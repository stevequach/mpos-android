package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.drv.beans.BusinessProfile;
import com.drv.common.Utils;
import com.drv.database.DatabaseManager;

public class BusinessProfileActivity extends BaseActivity {
	private EditText nameEditText;
	private EditText numberEditText;
	private EditText countryEditText;
	private EditText provinceEditText;
	private EditText addressEditText;
	private EditText phoneEditText;
	private EditText postalCodeEditText;
	private EditText cityEditText;
	private BusinessProfile profile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.business_profile);

		nameEditText = (EditText) findViewById(R.id.profileBusinessNameEditText);
		numberEditText = (EditText) findViewById(R.id.profileBusinessNumberEditText);
		addressEditText = (EditText) findViewById(R.id.profileAddressEditText);
		cityEditText = (EditText) findViewById(R.id.profileCityEditText);
		postalCodeEditText = (EditText) findViewById(R.id.profilePostalCodeEditText);
		phoneEditText = (EditText) findViewById(R.id.profilePhoneNumberEditText);
		countryEditText = (EditText) findViewById(R.id.editTextCountry);
		countryEditText.setOnKeyListener(null);
		countryEditText.setFocusable(false);
		countryEditText.setOnClickListener(this);
		// provinceSpiner = (Spinner) findViewById(R.id.profileProvinceSpinner);
		provinceEditText = (EditText) findViewById(R.id.editTextProvince);
		provinceEditText.setOnKeyListener(null);
		provinceEditText.setFocusable(false);
		provinceEditText.setOnClickListener(this);

		//this.getBusinessProfile();
	}

	private void getBusinessProfile() {
		long customerId = Utils.customerInfo.getId();
		DatabaseManager manager = DatabaseManager
				.getInstance(BusinessProfileActivity.this);
		profile = manager.getBusninessProfile(customerId);
		if (profile != null) {
			nameEditText.setText(profile.getName());
			numberEditText.setText(profile.getNumber());
			countryEditText.setText(profile.getCountry());
			provinceEditText.setText(profile.getProvince());
			addressEditText.setText(profile.getAddress());
			cityEditText.setText(profile.getCity());
			postalCodeEditText.setText(profile.getPostalCode());
			phoneEditText.setText(profile.getPhoneNumber());
		}
	}

	public void saveOnClick(View v) {
		Boolean isUpdate = true;
		if (profile == null) // Insert
		{
			profile = new BusinessProfile();
			isUpdate = false;
		}
		profile.setName(nameEditText.getText().toString());
		profile.setNumber(numberEditText.getText().toString());
		profile.setCountry(countryEditText.getText().toString());
		profile.setProvince(provinceEditText.getText().toString());
		profile.setAddress(addressEditText.getText().toString());
		profile.setCity(cityEditText.getText().toString());
		profile.setPostalCode(postalCodeEditText.getText().toString());
		profile.setPhoneNumber(phoneEditText.getText().toString());
		profile.setCustomerId(String.valueOf(Utils.customerInfo.getId()));

		DatabaseManager manager = DatabaseManager
				.getInstance(BusinessProfileActivity.this);
		if (isUpdate) {
			if (manager.updateBusinessProfile(profile)) {
				Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();
				this.finish();
			} else {
				Toast.makeText(this, "Updated Error", Toast.LENGTH_SHORT).show();
			}
		} else {
			if (manager.addBusinessProfile(profile)) {
				Toast.makeText(this, "Added new", Toast.LENGTH_SHORT).show();
				this.finish();
			} else {
				Toast.makeText(this, "Added Error", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		if (v == countryEditText) {
			Intent intent = new Intent();
			intent.setClass(BusinessProfileActivity.this,
					SelectCountryActivity.class);
			startActivityForResult(intent, 1);
		} else if (v == provinceEditText) {

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case 1: {
				countryEditText.setText(data.getStringExtra("countryname"));
			}
				break;

			default:
				break;
			}
		}
	}
}
