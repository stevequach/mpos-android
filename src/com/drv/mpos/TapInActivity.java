package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class TapInActivity extends BaseActivity {
	EditText pinEditText;
	EditText confirmPinEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tap_in);
		pinEditText = (EditText) findViewById(R.id.tapInPinEditText);
		confirmPinEditText = (EditText) findViewById(R.id.tapInConfirmPinEditText);
	}

	public void associateOnClick(View v) {
		finish();
	}
}
