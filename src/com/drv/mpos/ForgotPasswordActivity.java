package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPasswordActivity extends AnonymousBaseActivity{

	private EditText emailEditText;
	private Button retrievePasswordButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		emailEditText = (EditText)findViewById(R.id.editTextEmail);
		retrievePasswordButton = (Button)findViewById(R.id.buttonRetrievePassword);
		retrievePasswordButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//validate input fields
				String inputError = validateInputFields();
				//no error
				if(inputError == null){
					finish();
				}
				else{ //input error
					Toast.makeText(getApplicationContext(),
				               inputError, Toast.LENGTH_SHORT).show();
				}
			
			}
		});
	}
	protected String validateInputFields() {
		if(emailEditText.getText().toString().isEmpty()){
			return getString(R.string.forgot_password_empty_email);
		}
		return null;
	}

}
