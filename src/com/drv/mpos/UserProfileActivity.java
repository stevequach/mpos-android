package com.drv.mpos;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class UserProfileActivity extends BaseActivity {

	private ListView mainListView;
	private ArrayList<ItemData> itemList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_profile);
		this.initialData();
		mainListView = (ListView) findViewById(R.id.listViewMain);
		mainListView.setAdapter(new ItemListAdapter(UserProfileActivity.this,
				itemList));
		mainListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				switch (position) {
				case 0:// personal information
				{
					Intent intent = new Intent();
					intent.setClass(UserProfileActivity.this,
							PersonalInformationActivity.class);
					startActivity(intent);
				}
					break;
				case 1:// contact information
				{
					Intent intent = new Intent();
					intent.setClass(UserProfileActivity.this,
							ContactInformationActivity.class);
					startActivity(intent);
				}
					break;
				case 2:// password
				{
					Intent intent = new Intent();
					intent.setClass(UserProfileActivity.this,
							ChangePasswordActivity.class);
					startActivity(intent);
				}
					break;
				case 3:// Tap In
				{
					Intent intent = new Intent();
					intent.setClass(UserProfileActivity.this,
							TapInActivity.class);
					startActivity(intent);
				}
					break;

				default:
					break;
				}
			}
		});
	}

	class ItemListAdapter extends ArrayAdapter<ItemData> {

		public ItemListAdapter(Context context, List<ItemData> objects) {
			super(context, R.layout.user_profile_row, objects);
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(context);
		}

		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			Holder holder;
			ItemData item = this.getItem(position);
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.user_profile_row, null);
				holder = new Holder();
				holder.leftIconImageView = (ImageView) convertView
						.findViewById(R.id.imageViewLeftIcon);
				holder.titleTextView = (TextView) convertView
						.findViewById(R.id.textViewTitle);
				holder.rightArrowImageView = (ImageView) convertView
						.findViewById(R.id.imageViewRightArrow);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.titleTextView.setText(item.getTitleId());
			holder.leftIconImageView.setImageResource(item.getLeftIconId());
			holder.rightArrowImageView.setImageResource(item.getRightArrowId());
			return convertView;
		}

		private LayoutInflater inflater;
	}

	class ItemData {
		public ItemData() {

		}

		public ItemData(int leftIconId, int titleId, int rightArrowId) {
			super();
			this.leftIconId = leftIconId;
			this.titleId = titleId;
			this.rightArrowId = rightArrowId;
		}

		public int getLeftIconId() {
			return leftIconId;
		}

		public void setLeftIconId(int leftIconId) {
			this.leftIconId = leftIconId;
		}

		public int getTitleId() {
			return titleId;
		}

		public void setTitleId(int titleId) {
			this.titleId = titleId;
		}

		public int getRightArrowId() {
			return rightArrowId;
		}

		public void setRightArrowId(int rightArrowId) {
			this.rightArrowId = rightArrowId;
		}

		private int leftIconId;
		private int titleId;
		private int rightArrowId;
	}

	class Holder {
		public ImageView leftIconImageView;
		public TextView titleTextView;
		public ImageView rightArrowImageView;
	}

	protected void initialData() {
		itemList = new ArrayList<UserProfileActivity.ItemData>();

		itemList.add(new ItemData(R.drawable.pro_information,
				R.string.user_profile_personal_info, R.drawable.grayarrow));
		itemList.add(new ItemData(R.drawable.pro_contact_info,
				R.string.user_profile_contact_info, R.drawable.grayarrow));
		itemList.add(new ItemData(R.drawable.pro_password,
				R.string.user_profile_password, R.drawable.grayarrow));
		itemList.add(new ItemData(R.drawable.pro_tapin,
				R.string.user_profile_tap_in, R.drawable.grayarrow));

	}

}