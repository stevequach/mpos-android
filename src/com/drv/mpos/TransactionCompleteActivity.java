package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.drv.adapter.CustomBoldFontTextView;
import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.Transaction;
import com.drv.common.Constant;
import com.drv.common.Utils;

public class TransactionCompleteActivity extends BaseActivity {
	private CustomNormalFontTextView orderNumberTextView;
	private CustomNormalFontTextView dateTextView;
	private CustomNormalFontTextView timeTextView;
	private CustomNormalFontTextView statusTextView;
	private CustomNormalFontTextView methodTextView;
	private CustomNormalFontTextView nameOnCardTextView;
	private CustomNormalFontTextView cardTypeTextView;
	private CustomNormalFontTextView cardNumberTextView;
	private CustomNormalFontTextView subTotalTextView;
	private CustomNormalFontTextView taxTextView;
	private CustomNormalFontTextView tipTextView;
	private CustomBoldFontTextView totalTextView;
	private EditText emailEditText;
	private EditText phoneNumberEditText;

	private LinearLayout creditLinearLayout;
	private LinearLayout tipLinearLayout;
	private ImageView secureKeyImageView;
	private Transaction currentTransaction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.transaction_complete);
		orderNumberTextView = (CustomNormalFontTextView) findViewById(R.id.transactionOrderNumberTextView);
		dateTextView = (CustomNormalFontTextView) findViewById(R.id.transactionDateTextView);
		timeTextView = (CustomNormalFontTextView) findViewById(R.id.transactionTimeTextView);
		statusTextView = (CustomNormalFontTextView) findViewById(R.id.transactionStatusTextView);
		methodTextView = (CustomNormalFontTextView) findViewById(R.id.transactionMethodTextView);
		nameOnCardTextView = (CustomNormalFontTextView) findViewById(R.id.transactionNameOnCardTextView);
		cardTypeTextView = (CustomNormalFontTextView) findViewById(R.id.transactionCardTypeTextView);
		cardNumberTextView = (CustomNormalFontTextView) findViewById(R.id.transactionCardNumberTextView);
		subTotalTextView = (CustomNormalFontTextView) findViewById(R.id.transactionSubTotalTextView);
		taxTextView = (CustomNormalFontTextView) findViewById(R.id.transactionTaxTextView);
		tipTextView = (CustomNormalFontTextView) findViewById(R.id.transactionTipTextView);
		totalTextView = (CustomBoldFontTextView) findViewById(R.id.transactionTotalTextView);
		emailEditText = (EditText) findViewById(R.id.transactionEmailEditText);
		phoneNumberEditText = (EditText) findViewById(R.id.transactionMobilePhoneEditText);

		creditLinearLayout = (LinearLayout) findViewById(R.id.transactionCreditCardLinearLayout);
		tipLinearLayout = (LinearLayout) findViewById(R.id.transactionTipLinearLayout);
		secureKeyImageView = (ImageView) findViewById(R.id.transactionSecureKeyImageView);

		this.initData();
	}

	private void initData() {
		Intent intent = getIntent();
		currentTransaction = (Transaction) intent
				.getSerializableExtra("CurrentTransaction");
		if (currentTransaction.getPaymentMethod().equals(Constant.CREDIT_CARD)) {
			creditLinearLayout.setVisibility(View.VISIBLE);
			tipLinearLayout.setVisibility(View.VISIBLE);
			secureKeyImageView.setVisibility(View.VISIBLE);
		} else {
			creditLinearLayout.setVisibility(View.GONE);
			tipLinearLayout.setVisibility(View.GONE);
			secureKeyImageView.setVisibility(View.GONE);
		}
		
		methodTextView.setText(currentTransaction.getPaymentMethod());
		subTotalTextView.setText(Utils.formatAmountToString(currentTransaction.getSubTotal()));
		taxTextView.setText(Utils.formatAmountToString(currentTransaction.getTaxValue()));
		tipTextView.setText(Utils.formatAmountToString(currentTransaction.getTipValue()));
		totalTextView.setText(Utils.formatAmountToString(currentTransaction.getTotalAmount()));
	}

	public void sendReceiptOnClick(View v) {
		Intent intent = new Intent();
		intent.setClass(TransactionCompleteActivity.this,
				DashboardActivity.class);
		startActivity(intent);
	}
}
