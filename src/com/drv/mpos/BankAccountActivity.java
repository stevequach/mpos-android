package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

public class BankAccountActivity extends BaseActivity {

	private Spinner accountHolderSpinner;
	private Spinner transitNumberSpinner;
	private Spinner institutionSpinner;
	private Spinner accountNumberSpinner;
	private ImageView switchButton;
	private Button saveButton;
	
	private boolean onDeposit = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bank_account);
		saveButton = (Button)findViewById(R.id.saveButton);
		saveButton.setOnClickListener(this);
		
		accountHolderSpinner = (Spinner)findViewById(R.id.accountHolderSpinner);
		transitNumberSpinner = (Spinner)findViewById(R.id.transitNumberSpinner);
		institutionSpinner = (Spinner)findViewById(R.id.institutionNumberSpinner);
		accountNumberSpinner = (Spinner)findViewById(R.id.accountNumberSpinner);
		
		switchButton = (ImageView)findViewById(R.id.switchButton);
		switchButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == saveButton){
			finish();
		}
		else if(v ==  switchButton){
			if (onDeposit){
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.offswitch));
				onDeposit = false;
				accountHolderSpinner.setEnabled(false);
				transitNumberSpinner.setEnabled(false);
				institutionSpinner.setEnabled(false);;
				accountNumberSpinner.setEnabled(false);
			}
			else{
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.onswitch));
				onDeposit = true;
				accountHolderSpinner.setEnabled(true);
				transitNumberSpinner.setEnabled(true);
				institutionSpinner.setEnabled(true);
				accountNumberSpinner.setEnabled(true);				
			}
		}
		else{
			super.onClick(v);
		}
	}
}
