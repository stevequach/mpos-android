package com.drv.mpos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.drv.beans.Transaction;
import com.drv.common.Constant;
import com.drv.common.Utils;

public class DashboardActivity extends BaseActivity implements OnItemSelectedListener, OnClickListener  {
	
	private static final int NUMBER_LAST_DAYS = 5;

	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

	private GraphicalView mChartView;
	
	private TextView lastTransactionLabel;
	
	private TextView dateLabel;
	
	private TextView amountLabel;
	
	private TextView numberTextView;
	
	private TextView viewMoreTransactionTextView;
	
	private String[] accounts = {"Credit Card","Cash","Cheque"};	
	

	List<Transaction> transactionList;
	HashMap<String, List<Transaction>> map;
	Date startDate;
	Date endDate;
	float maxAmount;
	float minAmount;

	public void createSampleData() {
		transactionList = new ArrayList<Transaction>();

		for (int i = 0; i < 20; i++) {

			Random rn = new Random();
			int range = 30 - 5 + 1;
			int randomNum = rn.nextInt(range) + 5;
			float tax = randomNum * (float) Constant.TAX_RATE / 100;
//			int randomTip = rn.nextInt(range) + 1;

			Transaction tran = new Transaction();
			tran.setTransactionName("transaction name " + i);
			tran.setTransactionDesc("transaction description" + i);
			tran.setAmount(randomNum);
			
			tran.setTaxValue(tax);
//			tran.setTipValue(randomTip);
			tran.setTotalAmount(randomNum + tax);
			
			Date d = new Date();
			d.setDate(d.getDate() + randomNum);
			d.setHours(i);
			tran.setTransactionDate(d);
			transactionList.add(tran);
		}

		System.out.println("=========================");
		System.out.println("\n");

		for (Transaction transaction : transactionList) {
			System.out.println(transaction.getTransactionName());
			System.out.println(transaction.getAmount());
			System.out.println(transaction.getTransactionDate().toString());

			System.out.println("\n");
		}
		
		Utils.transactionList = transactionList;
	}

	private XYMultipleSeriesDataset getDateDataset() {
		//
		Collections.sort(transactionList);
		// Count number of days in the range date
		startDate = transactionList.get(0).getTransactionDate();
		endDate = transactionList.get(transactionList.size() - 1)
				.getTransactionDate();
		float lastAmount = transactionList.get(transactionList.size() - 1)
				.getAmount();
		
		String strLastTransaction = Utils.convertDateToString(endDate, "MMMM dd, yyyy @ hh:mm a z");
		strLastTransaction = strLastTransaction.substring(0, strLastTransaction.length()-6);
		lastTransactionLabel.setText(strLastTransaction);
		
		dateLabel.setText(strLastTransaction);
		amountLabel.setText("$" + Utils.formatAmountToString(lastAmount));
				
		startDate = Utils.resetTime(startDate);
		endDate = Utils.resetTime(endDate);

		groupDataByTransactionList(transactionList);

		System.out.println("map size:" + map.size());

		final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
		int diffInDays = (int) ((endDate.getTime() - startDate.getTime()) / DAY_IN_MILLIS);

		Log.d("diff in days:", Math.abs(diffInDays) + "");

		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		long startDateValue = startDate.getTime();

		List<double[]> arrXY = new ArrayList<double[]>();

		int m = 0;
		maxAmount = 0;
		minAmount = 0;

		for (int i = 0; i < transactionList.size(); i++) {

			Date currentDate = transactionList.get(i).getTransactionDate();
			float currentAmount = transactionList.get(i).getAmount();

			if (maxAmount < currentAmount) {
				maxAmount = currentAmount;
			}
			if (minAmount > currentAmount) {
				minAmount = currentAmount;
			}

			currentDate = Utils.resetTime(currentDate);

			String strDate = Utils.convertDateToString(currentDate, "MMddyyyy");

			int k = map.get(strDate).size();

			if (k > 1) {
				m++;
			} else {
				m = 0;
			}

			double diff = ((currentDate.getTime() - startDateValue) / DAY_IN_MILLIS);
			double rate = (double) 1 / (k + 1);

			if (k == 1) {
				diff = (diff + rate);
			} else {
				diff = (diff + rate * m);
			}

			System.out.println("\n");
			System.out.println("-------------k:" + k);
			System.out.println("start Date:" + startDate);
			System.out.println("current Date:" + currentDate);
			System.out.println("diff Date:" + diff);

			double[] arrDouble = new double[2];

			double tmpDate = startDateValue + diff * (TimeChart.DAY);
			float amount = transactionList.get(i).getAmount();

			arrDouble[0] = tmpDate;
			arrDouble[1] = (long) amount;
			arrXY.add(arrDouble);
			
			//mark X value for the transaction
			transactionList.get(i).setxAxis(tmpDate);
		}

		TimeSeries series = new TimeSeries("");
		for (int j = 0; j < arrXY.size(); j++) {
			double[] d = arrXY.get(j);
			// System.out.println("d1:" + d[0] + " Date Changed:" + new
			// Date((long) d[0]*1000));
			// System.out.println("d2:" + d[1]);
			series.add(new Date((long) d[0]), d[1]);

		}
		dataset.addSeries(series);

		return dataset;
	}

	private XYMultipleSeriesRenderer getRenderer() {
		
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setAxisTitleTextSize(15);
		
		renderer.setYLabels(0);
		renderer.setXLabelsColor(Color.BLACK);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setClickEnabled(true);
		renderer.setPointSize(5f);
		renderer.setShowLegend(false);
		renderer.setMargins(new int[] { 0, 0, 10, 20 });
		
		long minXPan = startDate.getTime() - 1 * TimeChart.DAY;
		long maxX = endDate.getTime() + 1 * TimeChart.DAY;
		long minX = maxX - NUMBER_LAST_DAYS * TimeChart.DAY;
		float minY = 0;
		float maxY = maxAmount + 10;

		renderer.setRange(new double[] { minX, maxX, minY, maxY });
		renderer.setPanEnabled(true, false);
		renderer.setPanLimits(new double[] { minXPan, maxX, minY, maxY });		

		System.out.println("x min date:" + new Date(minX));
		System.out.println("x max date:" + new Date(maxX));

		XYSeriesRenderer r = new XYSeriesRenderer();

		r.setColor(Color.BLUE);
		r.setPointStyle(PointStyle.CIRCLE);
		r.setFillPoints(true);
		r.setFillBelowLine(true);
		r.setFillBelowLineColor(getResources().getColor(R.color.main_color));
		r.setGradientEnabled(true);

		renderer.addSeriesRenderer(r);
		renderer.setApplyBackgroundColor(true);
		renderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));

		return renderer;
	}

	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		mDataset = (XYMultipleSeriesDataset) savedState
				.getSerializable("dataset");
	}

	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("dataset", mDataset);
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		setTheme(R.style.Theme_Holo_Light);
		// add spinner		
		Spinner spinner = (Spinner) findViewById(R.id.account_type_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout		
		 ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, accounts);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		
		viewMoreTransactionTextView = (TextView) findViewById(R.id.viewMoreTransactionTextView);
		viewMoreTransactionTextView.setOnClickListener(this);
		
		numberTextView = (TextView)findViewById(R.id.numberTextView);
        
		lastTransactionLabel = (TextView) findViewById(R.id.lastTransactionLabel);
		dateLabel = (TextView) findViewById(R.id.dateLabel);		
		amountLabel = (TextView) findViewById(R.id.amountLabel);
		
		
		createSampleData();
	}

	protected void onResume() {
		super.onResume();
		if (mChartView == null) {
			LinearLayout layout = (LinearLayout) findViewById(R.id.chart);

			mChartView = ChartFactory.getTimeChartView(this, getDateDataset(),
					getRenderer(), "dd/MM");

			mChartView.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView
							.getCurrentSeriesAndPoint();
					if (seriesSelection != null) {
						for (Transaction tran : transactionList) {
							double xAxis = tran.getxAxis();
							if(xAxis == seriesSelection.getXValue()){
								System.out.println("#### transaction selection:" + tran.getTransactionDate());
								String tranDate = Utils.convertDateToString(tran.getTransactionDate(), "MMMM dd, yyyy @ hh:mm a z");
								tranDate = tranDate.substring(0, tranDate.length()-6);
								dateLabel.setText(tranDate);
								amountLabel.setText("$"+Utils.formatAmountToString(tran.getAmount()));
							}
						}
					}
				}
			});
			layout.addView(mChartView, new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		} else {
			mChartView.repaint();
		}
	}

	private void groupDataByTransactionList(List<Transaction> transactionList) {
		List<Transaction> tmpList = new ArrayList<Transaction>();
		tmpList.add(transactionList.get(0));
		map = new HashMap<String, List<Transaction>>();
		map.put(Utils.convertDateToString(transactionList.get(0)
				.getTransactionDate(), "MMddyyyy"), tmpList);

		for (int i = 1; i < transactionList.size(); i++) {
			String text = Utils.convertDateToString(transactionList.get(i)
					.getTransactionDate(), "MMddyyyy");
			if (map.containsKey(text)) {
				// System.out.println("date exist:" + text);
				List<Transaction> tmpList1 = map.get(text);
				tmpList1.add(transactionList.get(i));
			} else {
				// System.out.println("date not exist:" + text);
				List<Transaction> tmpList2 = new ArrayList<Transaction>();
				tmpList2.add(transactionList.get(i));
				map.put(text, tmpList2);
			}
		}		
	}
	
	public void onItemSelected(AdapterView<?> parent, View view, 
	            int pos, long id) {
			System.out.println("view id:" + view.getId());
			System.out.println("pos:" + pos);
			System.out.println("id:" + id);
	        // An item was selected. You can retrieve the selected item using
	        // parent.getItemAtPosition(pos)
	}

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    
    @Override
    public void onClick(View v) {
    	// TODO Auto-generated method stub
    	if(v == viewMoreTransactionTextView){
    		Intent intent = new Intent();
    		intent.setClass(DashboardActivity.this, SaleHistoryActivity.class);
    		startActivity(intent);
    	}
    	super.onClick(v);
    }
}
