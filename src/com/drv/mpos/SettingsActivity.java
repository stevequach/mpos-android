package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.drv.adapter.CustomNormalFontTextView;

public class SettingsActivity extends AnonymousBaseActivity implements OnClickListener{
	private RadioGroup connectivityRadioGroup;
	private CustomNormalFontTextView advanceSettingsTextView;
	private LinearLayout advanceSettingLinearLayout;
	Button saveButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		connectivityRadioGroup = (RadioGroup)findViewById(R.id.RadioGroupConnectivityGroup);
		advanceSettingsTextView = (CustomNormalFontTextView)findViewById(R.id.textviewAdvanceSetting);
		advanceSettingLinearLayout = (LinearLayout)findViewById(R.id.LinearLayoutAdvanceSetting);
		saveButton = (Button)findViewById(R.id.buttonSave);
		
		advanceSettingsTextView.setOnClickListener(this);
		saveButton.setOnClickListener(this);
		connectivityRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId == R.id.radioButtonStandalone){
					advanceSettingLinearLayout.setVisibility(View.GONE);
				}
			}
		});
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == advanceSettingsTextView){
			switch (connectivityRadioGroup.getCheckedRadioButtonId()) {
			case R.id.radioButtonStandalone:
				break;
			case R.id.radioButtonLocal:
				setAdvanceLayoutVisibility();
				break;
			case R.id.radioButtonPublic:
				setAdvanceLayoutVisibility();
				break;
				
			default:
				break;
			}
			
		}
		else if(v == saveButton){
			finish();
		}
		else{
			super.onClick(v);
		}
	}
	public void setAdvanceLayoutVisibility(){
		if(advanceSettingLinearLayout.getVisibility() != View.VISIBLE){
			advanceSettingLinearLayout.setVisibility(View.VISIBLE);	
		}
		else{
			advanceSettingLinearLayout.setVisibility(View.GONE);	
		}
		
	}
}
