package com.drv.mpos;

import com.drv.beans.Product;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ManualEntryActivity extends BaseActivity {

	Button nextButton;
	EditText productNameEditText;
	EditText amountEditText;
	EditText productDescEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manual_entry);
		
		productNameEditText = (EditText) findViewById(R.id.productNameEditText);
		amountEditText = (EditText) findViewById(R.id.amountEditText);
		productDescEditText = (EditText) findViewById(R.id.productDescEditText);		

		nextButton = (Button) findViewById(R.id.nextButton);
		nextButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == nextButton) {
			
			if(productNameEditText.getText().toString().length()==0){
				Toast.makeText(getApplicationContext(), "Please enter product name",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if(amountEditText.getText().toString().length()==0){
				Toast.makeText(getApplicationContext(), "Please enter amount",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if(productDescEditText.getText().toString().length()==0){
				Toast.makeText(getApplicationContext(), "Please enter product description",
						Toast.LENGTH_SHORT).show();
				return;
			}			
			
			String productName = productNameEditText.getText().toString();
			String amount = amountEditText.getText().toString();
			String productDesc = productDescEditText.getText().toString();
			
			Intent intent = new Intent();
			intent.setClass(ManualEntryActivity.this,
					SelectPaymentActivity.class);
			
			Product productSelected = new Product();
			productSelected.setAmount(Integer.parseInt(amount));
			productSelected.setIconName(R.drawable.golfstuff);
			
			productSelected.setProductName(productName);
			productSelected.setProductDesc(productDesc);
			
			intent.putExtra("Product", productSelected);
			startActivity(intent);
		}
		else{
			super.onClick(v);
		}
	}
}
