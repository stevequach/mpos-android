package com.drv.mpos;

import com.drv.adapter.CustomNormalFontTextView;
import com.drv.beans.CustomerInfo;
import com.drv.database.DatabaseManager;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AnonymousBaseActivity{
	private CustomNormalFontTextView termAndCondLink; 
	private EditText firstNameEditText;
	private EditText lastNameEditText;
	private EditText emailEditText;
	private EditText passwordEditText;
	private EditText confirmPasswordEditText;
	private CheckBox agreeCheckBox;
	private Button registerButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		termAndCondLink = (CustomNormalFontTextView)findViewById(R.id.textViewTermAndCondiction);
		termAndCondLink.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		firstNameEditText = (EditText)findViewById(R.id.editTextFirstName);
		lastNameEditText = (EditText)findViewById(R.id.editTextLastName);
		emailEditText = (EditText)findViewById(R.id.editTextEmail);
		passwordEditText = (EditText)findViewById(R.id.editTextPassword);
		confirmPasswordEditText = (EditText)findViewById(R.id.editTextConfirmPassword);
		agreeCheckBox = (CheckBox)findViewById(R.id.checkBoxAgree);
		registerButton = (Button)findViewById(R.id.buttonRegister);
		
		registerButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//validate input fields
				String inputError = validateInputFields();
				//no error
				if(inputError == null){
					CustomerInfo ci = customerFromInputFields();
					DatabaseManager dbman = DatabaseManager.getInstance(RegisterActivity.this);
					String email = emailEditText.getText().toString();
					if(dbman.isEmailExisted(email)){
						Toast.makeText(getApplicationContext(),
								"Email existed! Please try another email.", Toast.LENGTH_SHORT).show();
					}
					if(dbman.registerCustomer(ci)){
						Toast.makeText(getApplicationContext(),
								"Register successfully.", Toast.LENGTH_SHORT).show();
						finish();
					}
					else{
						Toast.makeText(getApplicationContext(),
								"Failed to register", Toast.LENGTH_SHORT).show();
					}
				}
				else{ //input error
					Toast.makeText(getApplicationContext(),
				               inputError, Toast.LENGTH_SHORT).show();
				}
			}
		});	
	}
	protected String validateInputFields() {
		if(firstNameEditText.getText().toString().isEmpty()){
			return getString(R.string.register_empty_firstname);
		}
		if(lastNameEditText.getText().toString().isEmpty()){
			return getString(R.string.register_empty_lastname);
		}
		if(emailEditText.getText().toString().isEmpty()){
			return getString(R.string.register_empty_email);
		}
		if(passwordEditText.getText().toString().isEmpty()){
			return getString(R.string.register_empty_password);
		}
		if(confirmPasswordEditText.getText().toString().isEmpty()){
			return getString(R.string.register_empty_confirm_password);
		}
		if(passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString()) == false){
			return getString(R.string.register_password_not_match);
		}
		if(agreeCheckBox.isChecked() == false){
			return getString(R.string.register_not_agree_tnc);
		}
		return null;
	}
	protected CustomerInfo customerFromInputFields() {
		CustomerInfo ci = new CustomerInfo();
		ci.setFirstname(firstNameEditText.getText().toString());
		ci.setLastname(lastNameEditText.getText().toString());
		ci.setEmail(emailEditText.getText().toString());
		ci.setPassword(passwordEditText.getText().toString());
		return ci;
	}

}
