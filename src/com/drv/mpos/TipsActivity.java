package com.drv.mpos;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class TipsActivity extends BaseActivity implements OnClickListener {
	
	private ImageView switchButton;
	private ImageView upButton;
	private ImageView downButton;
	private TextView numberTextView;
	private Button saveButton;
	private Spinner spinner;
	
	private boolean onTip = true;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tips);
		
		switchButton = (ImageView)findViewById(R.id.switchButton);
		upButton = (ImageView)findViewById(R.id.upButton);
		downButton = (ImageView)findViewById(R.id.downButton);
		numberTextView = (TextView)findViewById(R.id.numberTextView);
		saveButton = (Button)findViewById(R.id.saveButton);
		
		spinner = (Spinner)findViewById(R.id.tipSpinner);
		
		switchButton.setOnClickListener(this);
		upButton.setOnClickListener(this);
		downButton.setOnClickListener(this);
		saveButton.setOnClickListener(this);
		
		switchButton.setImageDrawable(getResources().getDrawable(R.drawable.onswitch));
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stu
		if(v == switchButton){
			if (onTip){
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.offswitch));
				spinner.setEnabled(false);
				numberTextView.setEnabled(false);
				upButton.setEnabled(false);
				downButton.setEnabled(false);
				onTip = false;				
			}
			else{
				switchButton.setImageDrawable(getResources().getDrawable(R.drawable.onswitch));
				spinner.setEnabled(true);
				numberTextView.setEnabled(true);
				upButton.setEnabled(true);
				downButton.setEnabled(true);
				onTip = true;
			}
		}
		else if(v == upButton){
			processNumberButtonAction(true);
		}
		else if(v == downButton){
			processNumberButtonAction(false);
		}
		else if(v == saveButton){
			finish();
		}
		else{
			super.onClick(v);
		}
	}
	
	private void processNumberButtonAction(boolean isUp){
		int number = Integer.parseInt(numberTextView.getText().toString());		
		if(isUp){
			number++;		
		}
		else{
			if(number>1){
				number--;				
			}
		}
		numberTextView.setText(number+"");		
	}
	
}
