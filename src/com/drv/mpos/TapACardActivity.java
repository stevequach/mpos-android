package com.drv.mpos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TapACardActivity extends AnonymousBaseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tap_a_card);
	}

	public void tapOnClick(View v) {
		Intent intent = getIntent();
		String previousScreen = intent.getStringExtra("TapACardPreviousScreen");

		if (previousScreen.equals("AssociateContactlessCardScreen")) {
			Intent i = new Intent();
			i.setClass(this, VerifyPinActivity.class);
			startActivity(i);
		} else {
			Intent i = new Intent();
			i.setClass(this, DashboardActivity.class);
			startActivity(i);
		}
	}
}
