package com.drv.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;

import com.drv.beans.CustomerInfo;
import com.drv.beans.Transaction;

public class Utils {

	public static List<Transaction> transactionList;
	public static CustomerInfo customerInfo;

	public static String convertDateToString(Date date, String format) {
		DateFormat mySimpleDateFormat = new SimpleDateFormat(format);
		return mySimpleDateFormat.format(date);
	}

	public static Date resetTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Typeface getTypeface(Context ctx, String typeface) {
		Typeface tf = Typeface.createFromAsset(ctx.getAssets(), typeface);
		return tf;
	}

	public static String formatAmountToString(float amount) {
		return String.format("%1.2f", amount);
	}
}