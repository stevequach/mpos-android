package com.drv.common;

public class Constant {
	public static final float TAX_RATE = 13;
	public static final String TYPEFACE_NOVA_ALT_BOLD = "fonts/Proxima Nova Alt Bold.otf";
	public static final String TYPEFACE_NOVA_ALT_SEMI_BOLD = "fonts/Proxima Nova Alt Sbold.otf";
	public static final String UP = "UP";
	public static final String DOWN = "DOWN";
	public static final String CREDIT_CARD = "CREDIT CARD";
	public static final String CASH = "CASH";
	public static final String CHEQUE = "CHEQUE";
}
