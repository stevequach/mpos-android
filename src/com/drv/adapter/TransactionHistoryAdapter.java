package com.drv.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.drv.beans.Transaction;
import com.drv.common.Utils;
import com.drv.mpos.R;

public class TransactionHistoryAdapter extends BaseExpandableListAdapter {
	private Context context;
	private HashMap<String, List<Transaction>> transactionHashMap;
	private ArrayList<String> keys;

	public TransactionHistoryAdapter(Context c, ArrayList<String> keys,
			HashMap<String, List<Transaction>> hashMap) {
		this.context = c;
		this.transactionHashMap = hashMap;
		this.keys = keys;
	}

	@Override
	public Transaction getChild(int groupPosition, int childPosition) {
		String key = keys.get(groupPosition);
		List<Transaction> array = transactionHashMap.get(key);
		return array.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			// ROW INFLATION
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.history_row_expand, parent, false);
		}

		// Get item
		Transaction transaction = this.getChild(groupPosition, childPosition);
		CustomBoldFontTextView tv = (CustomBoldFontTextView) view
				.findViewById(R.id.rowExpandValue);
		tv.setText(transaction.getTransactionName());

		CustomNormalFontTextView detailsTextView = (CustomNormalFontTextView) view
				.findViewById(R.id.rowExpandDetailsValue);
		String value = transaction.getCurrency()
				+ Utils.formatAmountToString(transaction.getTotalAmount())
				+ "@ "
				+ Utils.convertDateToString(transaction.getTransactionDate(),
						"hh:mm aa");
		detailsTextView.setText(value);

		return view;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		String key = keys.get(groupPosition);
		return transactionHashMap.get(key).size();
	}

	@Override
	public String getGroup(int groupPosition) {
		return keys.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return transactionHashMap.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			// ROW INFLATION
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.history_row_group, parent, false);
		}

		ImageView indicatorImageView = (ImageView) view
				.findViewById(R.id.indicatorImageView);
		if (isExpanded)
			indicatorImageView.setImageResource(R.drawable.whitearrowdown);
		else
			indicatorImageView.setImageResource(R.drawable.whitearrow);
		// Get item
		String data = this.getGroup(groupPosition);
		CustomBoldFontTextView tv = (CustomBoldFontTextView) view
				.findViewById(R.id.rowGroupValue);

		tv.setText(data);

		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
}
