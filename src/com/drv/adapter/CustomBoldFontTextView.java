package com.drv.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.drv.common.Constant;

public class CustomBoldFontTextView extends TextView {

    public CustomBoldFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

   public CustomBoldFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

   public CustomBoldFontTextView(Context context) {
        super(context);
        init();
   }
   
   private void init() {
	   if(!isInEditMode()){
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
	    		   Constant.TYPEFACE_NOVA_ALT_BOLD);
	       setTypeface(tf);
	   }
   }
}
