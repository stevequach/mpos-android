package com.drv.adapter;


import com.drv.common.Constant;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomNormalFontTextView extends TextView {

      public CustomNormalFontTextView(Context context, AttributeSet attrs, int defStyle) {
          super(context, attrs, defStyle);
          init();
      }

     public CustomNormalFontTextView(Context context, AttributeSet attrs) {
          super(context, attrs);
          init();
      }

     public CustomNormalFontTextView(Context context) {
          super(context);
          init();
     }
     
     private void init() {
    	  if(!isInEditMode()){
	         Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
	      		   Constant.TYPEFACE_NOVA_ALT_SEMI_BOLD);
	         setTypeface(tf);
    	  }
     }
 }
