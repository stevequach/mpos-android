package com.drv.adapter;

import java.io.ByteArrayInputStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.drv.beans.CountryInfo;
import com.drv.database.DatabaseDefinition.Countries;
import com.drv.database.DatabaseManager;
import com.drv.mpos.R;

public class CountryCursorAdapter extends BaseAdapter implements Filterable{
	private Cursor mCursorData;
	private Context mContext;
	private LayoutInflater mInflater;
	private Filter newFilter;
	public CountryCursorAdapter(Context mContext) {
		super();
		this.mContext = mContext;
		this.mInflater = LayoutInflater.from(mContext);
		DatabaseManager dbManager = DatabaseManager.getInstance(mContext);
		releaseResource();
		this.mCursorData = dbManager.getAllCountryToCursor(null);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mCursorData != null){
			return mCursorData.getCount();
		}
		return 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return getCountryInfo(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if(mCursorData == null){
			return convertView;
		}
		ViewHolder holder = null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.select_country_row, null);
			holder.icon = (ImageView)convertView.findViewById(R.id.ImageViewCountryIcon);
			holder.name = (TextView)convertView.findViewById(R.id.TextViewCountryName);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		if(mCursorData.getCount() > position){
			//set data for holder
			mCursorData.moveToPosition(position);
			byte[] imageByteArray=mCursorData.getBlob(mCursorData.getColumnIndex(Countries.C_IMAGE));
			ByteArrayInputStream imageStream = new ByteArrayInputStream(imageByteArray);
			Bitmap theImage = BitmapFactory.decodeStream(imageStream);
			holder.icon.setImageBitmap(theImage);
			
			holder.name.setText(mCursorData.getString(mCursorData.getColumnIndex(Countries.C_COUNTRY_NAME)));
		}
		return convertView;
	}
		
	class ViewHolder{
		ImageView icon;
		TextView name;
	}

	public void releaseResource(){
		if(this.mCursorData != null){
			this.mCursorData.close();
			this.mCursorData = null;
		}
	}
	public Filter getFilter() {
		if (newFilter == null) {
			newFilter = new Filter() {
				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					notifyDataSetChanged();
					CountryCursorAdapter.this.notifyDataSetChanged();
				}

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					DatabaseManager dbManager = DatabaseManager.getInstance(mContext);
					releaseResource();
					mCursorData = dbManager.getAllCountryToCursor(constraint.toString());
					FilterResults newFilterResults = new FilterResults();
					newFilterResults.count = mCursorData.getCount();
					newFilterResults.values = mCursorData;
					return newFilterResults;
				}
			};
		}
		return newFilter;
	}
	public CountryInfo getCountryInfo(int position){
		CountryInfo ci = null;
		if(mCursorData != null && position < mCursorData.getCount()){
			mCursorData.moveToPosition(position);
			ci = new CountryInfo();
			ci.setName(mCursorData.getString(mCursorData.getColumnIndex(Countries.C_COUNTRY_NAME)));
			ci.setDialingCode(mCursorData.getString(mCursorData.getColumnIndex(Countries.C_ITU_T_TELEPHONE_CODE)));
		}
		return ci;
	}
}

