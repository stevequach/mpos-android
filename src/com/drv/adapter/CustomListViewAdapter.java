package com.drv.adapter;

import java.io.ByteArrayInputStream;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drv.beans.PickItem;
import com.drv.mpos.R;

public class CustomListViewAdapter extends ArrayAdapter<PickItem>{

	Context context;
	 
    public CustomListViewAdapter(Context context, int resourceId,
            List<PickItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtProductName;
        TextView txtProductDesc;
        TextView txtBalance;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        PickItem pickItem = getItem(position);
 
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.txtProductName = (TextView) convertView.findViewById(R.id.productName);
            holder.txtProductDesc = (TextView) convertView.findViewById(R.id.productDesc);
            holder.txtBalance = (TextView) convertView.findViewById(R.id.balance);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.txtProductName.setText(pickItem.getProductName());
        holder.txtProductDesc.setText(pickItem.getProductDesc());
        holder.txtBalance.setText(pickItem.getBalance());
        
        ByteArrayInputStream imageStream = new ByteArrayInputStream(pickItem.getProductImage());
		Bitmap theImage = BitmapFactory.decodeStream(imageStream);
	
        holder.imageView.setImageBitmap(theImage); 
        
        return convertView;
    }

}
