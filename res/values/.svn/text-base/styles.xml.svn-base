<resources xmlns:android="http://schemas.android.com/apk/res/android">

    <!--
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    -->
    <style name="AppBaseTheme" parent="android:Theme.Light">
        <!--
            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        -->
    </style>

    <!-- Application theme. -->
    <style name="AppTheme" parent="@android:style/Theme.Holo.Light">

        <!-- All customizations that are NOT specific to a particular API-level can go here. -->
        <!-- <item name="android:dropDownListViewStyle">@style/DropDownListView</item> -->

        <item name="android:actionBarWidgetTheme">@style/Theme.DropDown</item>
        <item name="android:radioButtonStyle">@style/RadioButton</item>
    </style>

    <style name="Theme.Holo.Light" parent="@android:style/Theme.Holo.Light">
        <item name="android:spinnerItemStyle">@style/SpinnerItem</item>
        <item name="android:actionBarWidgetTheme">@style/Theme.DropDown</item>
    </style>

    <style name="SpinnerItem" parent="@android:style/Widget.TextView.SpinnerItem">
        <item name="android:textColor">@color/white</item>
        <item name="android:textSize">13sp</item>
    </style>

    <style name="Theme.DropDown" parent="android:style/Theme.Holo.Light">
        <item name="android:dropDownListViewStyle">@style/DropDownListView</item>
    </style>

    <style name="DropDownListView">
        <item name="android:background">@drawable/settingsmenularger</item>
        <item name="android:popupBackground">@drawable/settingsmenularger</item>
        <item name="android:dropDownSelector">@drawable/settingsmenularger</item>
    </style>

    <style name="RadioButton" parent="@android:style/Widget.CompoundButton.RadioButton">
        <item name="android:button">@drawable/radio</item>
    </style>

    <style name="HeaderLayout">
        <item name="android:layout_width">fill_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:background">@drawable/subbar_768x1280</item>
        <item name="android:textSize">18sp</item>
        <item name="android:textColor">#FFFFFF</item>
        <item name="android:paddingLeft">10dp</item>
        <item name="android:gravity">center_vertical</item>
    </style>

    <style name="TextView">
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:textSize">14sp</item>
    </style>

    <style name="TextView.AlignRight">
        <item name="android:layout_width">0dp</item>
        <item name="android:layout_weight">1.0</item>
        <item name="android:gravity">right</item>
    </style>

    <style name="Button">
        <item name="android:textSize">20sp</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">center_horizontal</item>
        <item name="android:background">@drawable/largeredbutton</item>
        <item name="android:layout_marginTop">20dp</item>
    </style>

    <style name="Button_Medium">
        <item name="android:textSize">20sp</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">center_horizontal</item>
        <item name="android:background">@drawable/mediumredbutton</item>
        <item name="android:layout_marginTop">10dp</item>
        <item name="android:layout_marginLeft">10dp</item>
        <item name="android:layout_marginRight">10dp</item>
    </style>

    <style name="Button_Small">
        <item name="android:textSize">20sp</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">center_horizontal</item>
        <item name="android:background">@drawable/smallredbutton</item>
        <item name="android:layout_marginTop">10dp</item>
    </style>

    <style name="Button_Small2">
        <item name="android:textSize">20sp</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">center_horizontal</item>
        <item name="android:background">@drawable/smallredbutton2</item>
        <item name="android:layout_marginTop">10dp</item>
    </style>

    <style name="Button_Horizontal">
        <item name="android:textSize">20sp</item>
        <item name="android:textColor">@color/white</item>
        <item name="android:layout_width">wrap_content</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:layout_gravity">center_horizontal</item>
        <item name="android:background">@drawable/redbutton_horizontal</item>
        <item name="android:layout_marginTop">10dp</item>
    </style>

    <style name="LinkButton">
        <item name="android:textColor">@android:color/holo_red_dark</item>
        <item name="android:paddingTop">5dp</item>
        <item name="android:paddingBottom">5dp</item>
    </style>

    <style name="EditText">
        <item name="android:textSize">14sp</item>
        <item name="android:layout_width">fill_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:gravity">center|left</item>
        <item name="android:paddingLeft">5sp</item>
        <item name="android:paddingRight">5sp</item>
        <item name="android:background">@drawable/entryfield_1longer</item>
    </style>

    <style name="EditText_Transparent">
        <item name="android:textSize">14sp</item>
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:gravity">center|left</item>
        <item name="android:paddingLeft">5sp</item>
        <item name="android:paddingRight">5sp</item>
        <item name="android:background">@android:color/transparent</item>
    </style>

    <style name="TitleLabel">
        <item name="android:textSize">16sp</item>
        <item name="android:layout_width">fill_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:paddingLeft">10dp</item>
        <item name="android:paddingTop">15dp</item>
        <item name="android:paddingBottom">5dp</item>
    </style>

    <style name="RootView_Vertical">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:background">@drawable/background768x1280</item>
        <item name="android:focusable">true</item>
        <item name="android:focusableInTouchMode">true</item>
        <item name="android:orientation">vertical</item>
    </style>

    <style name="RootView_Horizontal">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:background">@drawable/background768x1280</item>
        <item name="android:orientation">horizontal</item>
    </style>

    <style name="ScrollView">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:layout_marginLeft">10dp</item>
        <item name="android:layout_marginRight">0dp</item>
        <item name="android:layout_marginTop">10dp</item>
        <item name="android:layout_marginBottom">10dp</item>
        <item name="android:scrollbarStyle">outsideInset</item>
        <item name="android:fillViewport">true</item>
    </style>

    <style name="Linear_Content">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">match_parent</item>
        <item name="android:layout_marginLeft">10dp</item>
        <item name="android:layout_marginRight">10dp</item>
        <item name="android:layout_marginTop">10dp</item>
        <item name="android:layout_marginBottom">10dp</item>
        <item name="android:background">@drawable/mainwidthbox</item>
    </style>

    <style name="ContentView">
        <item name="android:layout_width">match_parent</item>
        <item name="android:layout_height">wrap_content</item>
        <item name="android:paddingTop">10dp</item>
        <item name="android:paddingBottom">10dp</item>
        <item name="android:paddingLeft">10dp</item>
        <item name="android:paddingRight">10dp</item>
        <item name="android:background">@drawable/mainwidthbox</item>
    </style>

    <style name="FieldAlignTop">
        <item name="android:layout_marginTop">10dp</item>
    </style>

    <style name="FieldAlignBottom">
        <item name="android:layout_marginBottom">10dp</item>
    </style>

    <style name="MenuItem">
        <item name="android:layout_width">fill_parent</item>
        <item name="android:layout_height">0dp</item>
        <item name="android:layout_weight">0.25</item>
    </style>

    <style name="MenuImageButtonItem">
        <item name="android:layout_width">0dp</item>
        <item name="android:layout_height">fill_parent</item>
        <item name="android:layout_weight">0.5</item>
        <item name="android:background">@android:color/transparent</item>
    </style>

    <style name="WhiteTextColor">
        <item name="android:textColor">@color/white</item>
    </style>

</resources>